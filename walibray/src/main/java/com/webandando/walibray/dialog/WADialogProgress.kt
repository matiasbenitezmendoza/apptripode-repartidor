package com.webandando.walibray.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import com.budiyev.android.circularprogressbar.CircularProgressBar
import com.squareup.picasso.Picasso
import com.webandando.walibray.R
import de.hdodenhof.circleimageview.CircleImageView

class WADialogProgress(
    context: Context,
    urlImage: String?,
    drawable: Int?
) {

    private var dialog: AlertDialog? = null
    private var builder: AlertDialog.Builder? = null
    private var view: View? = null
    private var cpbProgress: CircularProgressBar? = null

    init {
        view = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null)

        val loadImg = view!!.findViewById<CircleImageView>(R.id.civ_carga_imagen)

        if (urlImage != null) {
            Picasso
                .get()
                .load(R.drawable.carga)
                .into(loadImg)
        } else if (drawable != null) {
            Picasso
                .get()
                .load(R.drawable.carga)
                .into(loadImg)
        }

        cpbProgress = view!!.findViewById(R.id.cpb_progress)
        builder = AlertDialog.Builder(context)
        dialog = builder?.setView(view)?.create()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)

    }

    fun show() {
        try {
            dialog?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hide() {
        dialog?.dismiss()
    }

    class Builder(var context: Context) {
        private var urlImage: String? = null
        private var drawable: Int? = null

        fun setUrlImg(urlImage: String) : Builder {
            this.drawable = null
            this.urlImage = urlImage
            return this
        }

        fun setDrawable(drawable: Int) : Builder {
            this.urlImage = null
            this.drawable = drawable
            return  this
        }

        fun build() : WADialogProgress {
            return WADialogProgress(context, urlImage, drawable)
        }
    }

}