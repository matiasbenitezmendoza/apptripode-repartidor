package com.webandando.walibray.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.webandando.walibray.R

class WADialog(
    context: Context,
    layout: Int,
    title: String?,
    titleResource: Int?,
    message: String?,
    messageResource: Int?,
    textButtontPositive: String?,
    textResourceButtonPositive: Int?,
    onClickListenerPositive: WADialog.OnClickListener?,
    textButtontNegative: String?,
    textResourceButtonNegative: Int?,
    onClickListenerNegative: WADialog.OnClickListener?,
    drawableIco: Int?,
    drawableButtonPositive: Int?,
    drawableButtonNegative: Int?,
    cancelable: Boolean = false
) {

    private var dialog: AlertDialog? = null
    private var builder:  AlertDialog.Builder? = null
    private var view: View? = null
    private var tvTitle: TextView
    private var tvMessage: TextView
    private var buttonPositive: Button
    private var buttonNegative: Button
    private var icoDialog: ImageView


    init {
        view = LayoutInflater.from(context).inflate(layout, null)
        tvTitle = view!!.findViewById(R.id.tvTitle)
        tvMessage = view!!.findViewById(R.id.tvMessage)
        buttonPositive = view!!.findViewById(R.id.btnPositive)
        buttonNegative = view!!.findViewById(R.id.btnNegative)
        icoDialog = view!!.findViewById(R.id.icoDialog)

        if (title != null && title.isNotEmpty()) tvTitle.text = title
        else if (titleResource != null)  tvTitle.text = context.getString(titleResource)
        else tvTitle.text = "Your title here.."
        if (message != null && message.isNotEmpty()) tvMessage.text = message
        else if (messageResource != null)  tvMessage.text = context.getString(messageResource)
        else tvMessage.text = "Your message here.."
        if (textButtontPositive != null && textButtontPositive.isNotEmpty()) buttonPositive.text = textButtontPositive
        else if (textResourceButtonPositive != null) buttonPositive.text = context.getString(textResourceButtonPositive)
        else buttonPositive.text = "Ok"
        if (textButtontNegative != null && textButtontNegative.isNotEmpty()) buttonNegative.text = textButtontNegative
        else if (textResourceButtonNegative != null) buttonNegative.text = context.getString(textResourceButtonNegative)
        else buttonNegative.visibility = View.GONE

        if (drawableIco != null) icoDialog.setImageDrawable(ContextCompat.getDrawable(context, drawableIco))
        if (drawableButtonPositive != null) buttonPositive.background = ContextCompat.getDrawable(context, drawableButtonPositive)
        if (drawableButtonNegative != null) buttonNegative.background = ContextCompat.getDrawable(context, drawableButtonNegative)
        if (drawableButtonNegative == null) buttonNegative.paintFlags = buttonNegative.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        buttonPositive.setOnClickListener {
            dismiss()
            onClickListenerPositive?.onClick()
        }

        buttonNegative.setOnClickListener {
            dismiss()
            onClickListenerNegative?.onClick()
        }

        builder = AlertDialog.Builder(context)
        dialog = builder?.setView(view)?.create()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        cancelable(cancelable)

    }

    fun cancelable(boolean: Boolean) {
        dialog?.setCancelable(boolean)
    }

    fun show() {
        dialog?.show()
    }

    fun dismiss() {
        dialog?.dismiss()
    }

    class Builder(var context: Context) {
        private var title: String? = null
        private var message: String? = null
        private var titleResource: Int? = null
        private var messageResource: Int? = null
        private var buttonPositive: Boolean = true
        private var buttonNegative: Boolean = false
        private var textButtonPositive: String? = null
        private var textButtonNegative: String? = null
        private var textResourceButtonPositive: Int? = null
        private var textResourceButtonNegative: Int? = null
        private var onButtonPositiveListener: WADialog.OnClickListener? = null
        private var onButtonNegativeListener: WADialog.OnClickListener? = null
        private var drawableIco: Int? = null
        private var drawableButtonPositive: Int? = null
        private var drawableButtonNegative: Int? = null
        private var cancelable: Boolean = false
        private var layout: Int = R.layout.dialog_container

        fun setTitle(title: String) : Builder {
            this.title = title
            this.titleResource = null
            return this
        }

        fun setTitle(titleResource: Int) : Builder {
            this.titleResource = titleResource
            this.title = null
            return this
        }

        fun setMessage(message: String) : Builder {
            this.message = message
            this.messageResource = null
            return this
        }

        fun setMessage(messageResource: Int) : Builder {
            this.messageResource = messageResource
            this.message = null
            return  this
        }

        fun succes() : Builder {
            this.drawableIco = R.drawable.ico_dialog_succes
            this.drawableButtonPositive = R.drawable.bg_button_positive
            return this
        }

        fun error() : Builder {
            this.drawableIco = R.drawable.ico_dialog_error
            this.drawableButtonPositive = R.drawable.bg_button_negative
            return this
        }

        fun setButtonPositive(text: String, onClickListener: OnClickListener) : Builder {
            this.textButtonPositive = text
            this.textResourceButtonPositive = null
            this.buttonPositive = true
            this.onButtonPositiveListener = onClickListener
            return this
        }

        fun setButtonPistivie(resourceText: Int, onClickListener: OnClickListener) : Builder {
            this.textButtonPositive = null
            this.textResourceButtonPositive = resourceText
            this.onButtonPositiveListener = onClickListener
            this.buttonNegative = true
            return this
        }

        fun setButtonNegative(text: String, onClickListener: OnClickListener) : Builder {
            this.textButtonNegative = text
            this.textResourceButtonNegative = null
            this.buttonNegative = true
            this.onButtonNegativeListener = onClickListener
            return this
        }

        fun setButtonNegative(resourceText: Int, onClickListener: OnClickListener) : Builder {
            this.textButtonNegative = null
            this.textResourceButtonNegative = resourceText
            this.onButtonNegativeListener = onClickListener
            this.buttonNegative = true
            return this
        }

        fun setIcoDrawable(drawable: Int) : Builder {
            this.drawableIco = drawable
            return this
        }

        fun setButtonPositiveDrawable(drawable: Int) : Builder {
            this.drawableButtonPositive = drawable
            return this
        }

        fun setButtonNegativeDrawable(drawable: Int) : Builder {
            this.drawableButtonNegative = drawable
            return this
        }

        fun setCancelable(cancelable: Boolean) : Builder {
            this.cancelable = cancelable
            return this
        }

        fun setView(layout: Int) : Builder {
            this.layout = layout
            return this
        }

        fun build() : WADialog {
            return WADialog(context,
                layout,
                title,
                titleResource,
                message,
                messageResource,
                textButtonPositive,
                textResourceButtonPositive,
                onButtonPositiveListener,
                textButtonNegative,
                textResourceButtonNegative,
                onButtonNegativeListener,
                drawableIco,
                drawableButtonPositive,
                drawableButtonNegative,
                cancelable
            )
        }

    }

    interface OnClickListener {
        fun onClick()
    }
}