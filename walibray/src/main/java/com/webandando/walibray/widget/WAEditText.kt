package com.webandando.walibray.widget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.EditText
import com.webandando.walibray.R

class WAEditText(context: Context?, attrs: AttributeSet?) : EditText(context, attrs) {

    private var typeFont: String? = "regular"
    init {

        if (context != null) {
/*
            val array: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.wa_components)
            typeFont = array.getString(R.styleable.wa_components_typefont)

            typeface = when(typeFont?.toUpperCase()) {
                REGULAR -> Typeface.createFromAsset(context.assets, "fonts/opensans_regular.ttf")
                LIGHT -> Typeface.createFromAsset(context.assets, "fonts/opensans_light.ttf")
                SEMIBOLD -> Typeface.createFromAsset(context.assets, "fonts/opensans_semibold.ttf")
                BOLD -> Typeface.createFromAsset(context.assets, "fonts/opensans_bold.ttf")
                else -> Typeface.createFromAsset(context.assets, "fonts/opensans_regular.ttf")
            }*/
        }
    }
    companion object {
        const val REGULAR   = "REGULAR"
        const val LIGHT     = "LIGHT"
        const val SEMIBOLD  = "SEMIBOLD"
        const val BOLD      = "bold"
    }

}