package com.webandando.tripode.ui.fragments_menu.perfil

import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView

interface MenuPerfilContract {

    interface View : BaseView<Presenter> {
        fun muestraInformacionUsuario()
        fun irALogin()
    }

    interface Presenter : BasePresenter {
        fun cerrarSesion()
    }
}