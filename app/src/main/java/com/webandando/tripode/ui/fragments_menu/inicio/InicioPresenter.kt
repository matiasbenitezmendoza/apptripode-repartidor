package com.webandando.tripode.ui.fragments_menu.inicio

import android.util.Log
import com.webandando.tripode.data.pedido.Pedido



class InicioPresenter( val mPedidoView : InicioContract.View,
                         val mPedidoInteractor: InicioInteractor) : InicioContract.Presenter, InicioInteractor.Callback {

    init {
        mPedidoView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
       // mPedidoView.muestraMensajesSuccess("desde start")

        Log.d("TAG", "init pedidos")

       mPedidoInteractor.getPedidos(this)

    }

    override fun goDetallePedido(pedido: Pedido) {

        // mPedidoInteractor.singupUser(user, this)
    }


    override fun onNetworkConnectionFailed() {
        mPedidoView.muestraErrorEnConexion()
    }


    override fun onPedidosSuccess(msg: String) {
        mPedidoView.muestraMensajesSuccess(msg)
    }

    override fun onPedidosFailed(err: String) {
        mPedidoView.muestraErrorDePeticion(err)
    }


    override fun onRequestFailed(err: String) {
        mPedidoView.muestrarErrorAlCargar(err)
    }

    override fun onRequestPedidosSucces(pedidos: ArrayList<Pedido>) {
        mPedidoView.agregarPedidosFilterList(pedidos)
    }



}