package com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus
import android.content.Context
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.pedido.source.PedidoService
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener

class DetallePedidoPresenter(val mDetallePedidoView : DetalleContract.View, val pedido: Pedido, val context: Context)
    : DetalleContract.Presenter {

    private val service: PedidoService = PedidoService.instance(context)

    init {
        mDetallePedidoView.setPresenter(this)
    }

    override fun start() {
        mDetallePedidoView.showPedido(pedido)
    }


    override fun irUbicacion() {

    }

    override fun irActualizar() {

    }

    override fun iniciarRuta(pedido: Pedido) {
        service.putRuta(pedido, object : ResponseListener<ResponseData<Pedido>> {

            override fun onSuccess(response: ResponseData<Pedido>) {
                mDetallePedidoView.showActualizado(response.data as Pedido)
            }

            override fun onError(t: Throwable) {
                mDetallePedidoView.showErrorActualizado(t.message!!)

            }

        })
    }

}