package com.webandando.tripode.ui.recover

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.webandando.tripode.R
import com.webandando.tripode.ui.login.LoginActivity

class RecoverActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover_pass)

        val fragment = RecoverFragment.getInstance() as RecoverFragment
        supportFragmentManager.beginTransaction()
            .add(R.id.recover_container, fragment)
            .commit()

        val loginInteractor = RecoverInteractor(applicationContext)
        RecoverPresenter(fragment, loginInteractor)
    }

    override fun onBackPressed() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

}