package com.webandando.tripode.ui.fragments_menu.historial_pedidos


import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView
import com.webandando.tripode.data.pedido.Pedido


interface HPedidosContract {

    interface View : BaseView<Presenter> {
        fun muestraProgress(show: Boolean)
        fun muestraErrorEnConexion()
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)
        fun muestraMensajesSuccess(mensaje: String)
        fun agregarPedidosFilterList(pedidos: ArrayList<Pedido>)

    }

    interface Presenter : BasePresenter {
        fun goDetallePedido(pedido: Pedido)
        fun getPedidos(limit: Int, offset: Int)

    }


}