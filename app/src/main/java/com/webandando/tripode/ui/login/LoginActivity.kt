package com.webandando.tripode.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.webandando.tripode.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val loginFragment = LoginFragment.getInstance() as LoginFragment
        supportFragmentManager.beginTransaction()
                .add(R.id.login_container, loginFragment)
                .commit()

        val loginInteractor = LoginInteractor(this@LoginActivity)
        LoginPresenter(loginFragment, loginInteractor)
    }
}