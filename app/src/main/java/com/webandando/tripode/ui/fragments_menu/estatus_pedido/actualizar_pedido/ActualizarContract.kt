package com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido


import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView
import com.webandando.tripode.data.pedido.Pedido

interface ActualizarContract {

    interface View : BaseView<Presenter> {
        fun setOrden(pedido: Pedido)
        fun showPedido(pedido: Pedido)
        fun showCalificacion(pedido: Pedido)
        fun irAtras()
        fun muestraErrorEnConexion()
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)

    }

    interface Presenter : BasePresenter {
        fun actualizar()
        fun actualizarImagen(imagen: String)
        fun actualizarComentario(comentario_repartidor: String, calificacion_repartidor: String)

    }
}
