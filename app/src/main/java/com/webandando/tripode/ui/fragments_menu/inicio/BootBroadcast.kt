package com.webandando.tripode.ui.fragments_menu.inicio

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class BootBroadcast : BroadcastReceiver() {


    private lateinit var database: DatabaseReference
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onReceive(ctx: Context, intent: Intent) {

        database = FirebaseDatabase.getInstance().reference
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)
        var latlng  = java.util.HashMap<String, Any>()
        latlng.put("reinicio", "celular reiniciado")
        var es = "logs/"
        database.child(es).push().setValue(latlng)

        val startIntent = Intent(ctx, ActualizarService::class.java)
        ctx.startService(startIntent)
    }

}