package com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.residencial.Residencial
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleContract
import com.webandando.tripode.util.lib.FragmentChildNotFab
import com.webandando.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_mapa.*
import kotlinx.android.synthetic.main.item_cardview_cliente.*

class UbicacionPedidoFragment : FragmentChildNotFab(), OnMapReadyCallback,  UbicacionPedidoContract.View {

    private var gMap: GoogleMap? = null
    private var mapView: MapView? = null
    private var vw: View? = null
    private var mPresenter: UbicacionPedidoContract.Presenter? = null
    private var residencial: Residencial? = null
    private var  lat = 19.3204968
    private var  lon = -99.1526134

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_mapa, container, false)
        mapView = vw!!.findViewById(R.id.map)
        mPresenter?.start()
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnBackMap.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {

        googleMap.setMapStyle(
            MapStyleOptions("[\n" +
                    "    {\n" +
                    "        \"stylers\": [\n" +
                    "            {\n" +
                    "                \"hue\": \"#007fff\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"saturation\": 89\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    },\n" +
                    "    {\n" +
                    "        \"featureType\": \"water\",\n" +
                    "        \"stylers\": [\n" +
                    "            {\n" +
                    "                \"color\": \"#ffffff\"\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    },\n" +
                    "    {\n" +
                    "        \"featureType\": \"administrative.country\",\n" +
                    "        \"elementType\": \"labels\",\n" +
                    "        \"stylers\": [\n" +
                    "            {\n" +
                    "                \"visibility\": \"off\"\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "             ]"
            )
        )

        gMap = googleMap

        gMap!!.addMarker(
            MarkerOptions()
                .position(LatLng(lat, lon))
                .title("")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_ft))
        )

        val cameraPosition = CameraPosition.builder()
            .target(LatLng( lat,lon))
            .zoom(12f)
            .bearing(0f)
            .tilt(7f)
            .build()
        gMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }


    override fun setPresenter(presenter: UbicacionPedidoContract.Presenter) {
        mPresenter = presenter
    }

    override fun muestrarErrorAlCargar(mensaje: String) {

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(mensaje)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun muestraErrorDePeticion(mensaje: String) {

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(mensaje)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun mostratMapa(residencial: Residencial, pedido: Pedido) {
        num_orden.text = "#"+pedido.orden
        nombreCliente.text = pedido.cliente
        if(residencial != null && residencial.latitud!! != 0.0){
            lat = residencial.latitud!!
            lon = residencial.longitud!!
        }

        if (mapView != null) {
            mapView!!.onCreate(null)
            mapView!!.onResume()
            mapView!!.getMapAsync(this)
        }
    }

    companion object {

        private var fragment: Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = UbicacionPedidoFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }
}