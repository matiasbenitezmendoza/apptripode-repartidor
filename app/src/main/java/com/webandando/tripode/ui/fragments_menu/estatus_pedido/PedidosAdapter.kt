package com.webandando.tripode.ui.fragments_menu.estatus_pedido

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import kotlin.math.absoluteValue

class PedidosAdapter(var pedidos: ArrayList<Pedido>, var listenerView: ListenerView)
    : RecyclerView.Adapter<PedidosAdapter.ViewHolder>() {
    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val vw = inflater.inflate(R.layout.item_pedido, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return pedidos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = pedidos[position]
        holder.bind(item)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ordenPedido : TextView = itemView.findViewById(R.id.tvTitleOrden)
        var fechaPedido : TextView = itemView.findViewById(R.id.tvFecha)
        var statusPedido: TextView = itemView.findViewById(R.id.TvStatusPedido)
        var horaPedido: TextView = itemView.findViewById(R.id.tvHora)
        var Pedido: TextView = itemView.findViewById(R.id.tvTitlePedido)

        fun bind(item: Pedido) {
            itemView.setOnClickListener { listenerView.onClick(item) }
            ordenPedido.text = "#"+item.orden
            fechaPedido.text = item.fecha
            horaPedido.text = item.hora
            Pedido.text = "Pedido"
            var s: String? = item.status_pedido.toString()

            if (s != null) {
                if(s != "3"){
                    statusPedido.text = context.getString(R.string.status_1)
                }else{
                    statusPedido.text = context.getString(R.string.status_2)
                }
            }

        }
    }

    interface ListenerView {
        fun onClick(item: Pedido)
    }
}
