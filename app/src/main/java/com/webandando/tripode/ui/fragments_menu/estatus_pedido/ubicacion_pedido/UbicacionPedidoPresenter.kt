package com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido

import android.content.Context
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.pedido.source.PedidoService
import com.webandando.tripode.data.residencial.Residencial
import com.webandando.tripode.data.residencial.source.ResidencialService
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener

class UbicacionPedidoPresenter(val mDetallePedidoView : UbicacionPedidoContract.View, val pedido: Pedido, val context: Context)
    : UbicacionPedidoContract.Presenter {

    private val service: ResidencialService = ResidencialService.instance(context)

    init {
        mDetallePedidoView.setPresenter(this)
    }

    override fun start() {
          getResidencial(pedido.residencial!!)
    }


    override fun getResidencial(residencial: String) {
        service.get(residencial, object : ResponseListener<ResponseData<Residencial>> {

            override fun onSuccess(response: ResponseData<Residencial>) {
                mDetallePedidoView.mostratMapa(response.data as Residencial, pedido)
            }
            override fun onError(t: Throwable) {
                mDetallePedidoView.muestraErrorDePeticion(t.message!!)
            }
        })
    }

}