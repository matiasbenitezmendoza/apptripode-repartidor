package com.webandando.tripode.ui.login

import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView

interface LoginContract {

    interface View : BaseView<Presenter> {
        fun muestraErrorEnCorreo(error: String)
        fun muestraErrorEnPasswd(error: String)
        fun muestrarErrorEnNetwork()
        fun muestrarErrorDeLogin(error: String)
        fun muestraTutorial()
        fun muestraRecuperarPass()
        fun muestraRegistrarUsuario()
    }

    interface Presenter : BasePresenter {
        fun intentaLoginPorCorreo( correo: String, passwd: String)
        fun intentaLoginPorFacebook()
    }
}