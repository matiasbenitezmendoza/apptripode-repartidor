package com.webandando.tripode.ui.recover

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.webandando.tripode.R
import com.webandando.tripode.ui.login.LoginActivity
import com.webandando.tripode.util.lib.TextDrawable
import kotlinx.android.synthetic.main.fragment_recover_pass.*

class RecoverFragment : Fragment(), RecoverContract.View {
    private var mPresenter: RecoverContract.Presenter? = null
    private lateinit var cntx: Context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recover_pass, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cntx = context!!
        setEvents()
        mPresenter?.start()
    }

    private fun setEvents() {
        btnBack.setOnClickListener { irALogin() }
        btnRegistrate.setOnClickListener { irAregistro() }
        btnEnviar.setOnClickListener { mPresenter?.intentaEnviarPeticion(inputCorreo.text.toString()) }
    }

    override fun errorEnConexion() {
        Snackbar
            .make(container, cntx.getText(R.string.msg_sin_conexion), Snackbar.LENGTH_LONG)
            .show()
    }

    override fun mostrarErrorEnCorreo(msg: String) {
        showInputError(inputCorreo, msg)
    }

    override fun mostrarMensajeDeError(msg: String) {
        tvInfo.text = msg
        tvInfo.setTextColor(ContextCompat.getColor(cntx, R.color.mid_green))
        ivInfo.setImageDrawable(ContextCompat.getDrawable(cntx, R.drawable.ico_error))
        ivInfo.visibility = View.VISIBLE

        btnRegistrate.visibility = View.VISIBLE
        btnBack.visibility = View.GONE
    }

    override fun mostrarMensajeSucces(msg: String) {
        tvInfo.text = msg
        tvInfo.setTextColor(ContextCompat.getColor(cntx, R.color.scarlet))
        ivInfo.setImageDrawable(ContextCompat.getDrawable(cntx, R.drawable.ico_success))
        ivInfo.visibility = View.VISIBLE

        btnBack.visibility = View.VISIBLE
        btnRegistrate.visibility = View.GONE
    }

    override fun irALogin() {
        (activity as RecoverActivity).onBackPressed()
    }

    override fun irAregistro() {
        /*val intent = Intent(activity, RegistroActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)*/
    }

    override fun setPresenter(presenter: RecoverContract.Presenter) {
        this.mPresenter = presenter
    }

    private fun showInputError(editText: EditText, error: String) {
        editText.background = ContextCompat.getDrawable(cntx, R.drawable.input_white_error)
        editText.setCompoundDrawables(null, null, TextDrawable(cntx, error), null)
    }

    companion object {
        fun getInstance() : Fragment {
            return RecoverFragment()
        }
    }
}