package com.webandando.tripode.ui.fragments_menu.historial_pedidos.detalle_estatus
import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView
import com.webandando.tripode.data.pedido.Pedido

interface DetalleContract {

    interface View : BaseView<Presenter> {
        fun showPedido(pedido: Pedido)
        fun showNetworkError()
        fun irAtras()

    }

    interface Presenter : BasePresenter {
        fun irUbicacion()
        fun irActualizar()

    }
}
