package com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido


import com.webandando.tripode.data.pedido.Pedido


class ActualizarPedidoPresenter(val mActualizarPedidoView : ActualizarContract.View, val mPedidoInteractor: ActualizarPedidoInteractor,  val pedido: Pedido)
    : ActualizarContract.Presenter, ActualizarPedidoInteractor.Callback {

    init {
        mActualizarPedidoView.setPresenter(this)
    }

    override fun start() {
       mActualizarPedidoView.setOrden(pedido)
    }

    override fun actualizar() {
        pedido.status_pedido = 3
        mPedidoInteractor.putPedido(pedido, this)
    }

    override fun actualizarImagen(imagen : String) {
        val id_pedido : Int = pedido.id!!
        mPedidoInteractor.putPedidoImagen(imagen, id_pedido, this)
    }

    override fun actualizarComentario(comentario_repartidor : String, calificacion_repartidor : String) {
        pedido.comentarios_repartidor = comentario_repartidor
        pedido.calificacion_repartidor = calificacion_repartidor
        mPedidoInteractor.putComentarios(pedido, this)
    }

    override fun onNetworkConnectionFailed() {
        mActualizarPedidoView.muestraErrorEnConexion()
    }

    override fun onRequestFailed(err: String) {
        mActualizarPedidoView.muestrarErrorAlCargar(err)
    }

    override fun onRequestPedidoSucces(pedido: Pedido) {
        mActualizarPedidoView.showPedido(pedido)
    }

    override fun onRequestScoreSucces(pedido: Pedido) {
        mActualizarPedidoView.showCalificacion(pedido)
    }

}