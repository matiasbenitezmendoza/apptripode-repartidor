package com.webandando.tripode.ui.recover

import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView

interface RecoverContract {

    interface View : BaseView<Presenter> {
        fun mostrarMensajeDeError(msg: String)
        fun mostrarMensajeSucces(msg: String)
        fun mostrarErrorEnCorreo(msg: String)
        fun irALogin()
        fun irAregistro()
        fun errorEnConexion()
    }

    interface Presenter : BasePresenter {
        fun intentaEnviarPeticion(correo: String)
    }
}