package com.webandando.tripode.ui.fragments_menu.perfil

import android.content.Context

class MenuPerfilInteractor (private val context: Context) {

    fun cerrarSesion(callback: Callback) {
        // proceso de cerrar sesion
        callback.onLogOut()
    }

    interface Callback {
        fun onLogOut()
    }
}