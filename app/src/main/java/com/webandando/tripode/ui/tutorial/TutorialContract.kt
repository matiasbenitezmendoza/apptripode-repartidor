package com.webandando.tripode.ui.tutorial

import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView

interface TutorialContract {

    interface View : BaseView<Presenter> {
        fun mostrarSiguientePaso()
        fun mostrarInicio()
    }

    interface Presenter : BasePresenter {

    }
}