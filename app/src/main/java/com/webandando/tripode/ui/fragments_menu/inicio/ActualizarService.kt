package com.webandando.tripode.ui.fragments_menu.inicio

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Handler
import android.os.IBinder
import android.util.Log
import java.util.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.database.*
import com.webandando.tripode.data.mapa.Mapa
import com.webandando.tripode.data.session.SessionPrefs
import kotlin.concurrent.schedule
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.webandando.tripode.R
import com.webandando.tripode.MainActivity
import android.content.IntentFilter





class ActualizarService : Service() {

    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    private lateinit var database: DatabaseReference
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var ruta : Boolean = true
    private var screenOnOffReceiver: BootBroadcast? = null

    override fun onCreate() {
        super.onCreate()
        database = FirebaseDatabase.getInstance().reference
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        var id_repartidor = SessionPrefs.getInstance(this).getIdUsuario().toString()
        var orden = SessionPrefs.getInstance(this).getOrden()

        /*
        val screenStateFilter = IntentFilter()
        screenStateFilter.addAction(Intent.ACTION_BOOT_COMPLETED)
        screenStateFilter.addAction(Intent.ACTION_REBOOT)
        registerReceiver(screenOnOffReceiver, screenStateFilter)*/

        val channelId = "TripodeRepartidor"
        var notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT)
            notificationChannel.description = channelId
            notificationChannel.setSound(null, null)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        val mBuilder = NotificationCompat.Builder(applicationContext, channelId)
            .setSmallIcon(R.mipmap.ic_launcher_foreground) // notification icon
            .setContentTitle("En Ruta ...") // title for notification
            .setContentText("Pedido #"+orden)// message for notification

        /*
        val intent = Intent(applicationContext,  MainActivity::class.java)
        val pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(pi)
        */

        startForeground(1, mBuilder.build())

        getubicacion(id_repartidor, orden)
        return START_STICKY
    }

    override fun onDestroy() {
        //unregisterReceiver(screenOnOffReceiver)
        stopForeground(true)
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onLowMemory() {
        //   Log.i(TAG, "onLowMemory()")
    }

    @SuppressLint("MissingPermission")
    fun getubicacion(id_repartidor : String, orden: String){

        var ruta_url = "repartidores/"+id_repartidor+"/"+orden
        var pedido_url = "pedidos/"+orden
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var punto: Mapa? = dataSnapshot.getValue(Mapa::class.java)
                if(punto != null){
                    fusedLocationClient.lastLocation
                                    .addOnSuccessListener { location : Location? ->
                                        if(location != null){
                                            var latlng  = java.util.HashMap<String, Any>()
                                            latlng.put("latitud",location.latitude)
                                            latlng.put("longitud", location.longitude)
                                            database.child(ruta_url).setValue(latlng)
                                        }
                                    }
                    Timer().schedule(20000){
                        getubicacion(id_repartidor, orden)
                    }

                }else{

                    var pedido  = java.util.HashMap<String, Any>()
                    pedido.put("orden", orden)
                    pedido.put("repartidor", id_repartidor)
                    pedido.put("status", "Pedido Entregado")
                    database.child(pedido_url).setValue(pedido)
                    ruta = false
                    stopSelf()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        database.child(ruta_url).addListenerForSingleValueEvent(postListener)

    }


}