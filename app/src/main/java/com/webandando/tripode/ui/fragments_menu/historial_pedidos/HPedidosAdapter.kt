package com.webandando.tripode.ui.fragments_menu.historial_pedidos

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import java.text.DateFormat

class HPedidosAdapter(var pedidos: ArrayList<Pedido>, var listenerView: ListenerView)
    : RecyclerView.Adapter<HPedidosAdapter.ViewHolder>() {
    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val vw = inflater.inflate(R.layout.item_hpedido, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return pedidos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = pedidos[position]
        holder.bind(item)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ordenPedido : TextView = itemView.findViewById(R.id.tvHTitlePedido)
        var statusPedido: TextView = itemView.findViewById(R.id.tvHStatus)
        var Cliente: TextView = itemView.findViewById(R.id.tvHCliente)

        fun bind(item: Pedido) {
            itemView.setOnClickListener { listenerView.onClick(item) }
            ordenPedido.text = "#"+item.orden
            Cliente.text = item.cliente
            var s: String? = item.status_pedido.toString()

            if (s != null) {
                if(s != "3"){
                    statusPedido.text = context.getString(R.string.status_1)
                }else{
                    statusPedido.text = context.getString(R.string.status_2)
                }
            }

        }
    }

    interface ListenerView {
        fun onClick(item: Pedido)
    }
}
