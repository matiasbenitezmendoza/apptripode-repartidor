package com.webandando.tripode.ui.fragments_menu.historial_pedidos.detalle_estatus
import com.webandando.tripode.data.pedido.Pedido

class DetallePedidoPresenter(val mDetallePedidoView : DetalleContract.View, val pedido: Pedido)
    : DetalleContract.Presenter {

    init {
        mDetallePedidoView.setPresenter(this)
    }

    override fun start() {
        mDetallePedidoView.showPedido(pedido)
    }


    override fun irUbicacion() {

    }

    override fun irActualizar() {

    }

}