package com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido


import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.webandando.tripode.MainActivity
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.session.SessionPrefs
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.PedidosFragment
import com.webandando.tripode.util.lib.FragmentChildNotFab
import com.webandando.walibray.dialog.WADialog
import com.webandando.walibray.dialog_form.WADialogForm
import kotlinx.android.synthetic.main.fragment_form_status.*
import java.io.File


class ActualizarPedidoFragment : FragmentChildNotFab(), ActualizarContract.View {

    private lateinit var pedido: Pedido
    private var mPresenter: ActualizarContract.Presenter? = null
    private lateinit var vw: View

    private var score: String = ""
    private var scorem: String = ""

    private var dialogForm: WADialogForm? = null

    private var mCurrentPhotoPath: String = ""
    private var mPhotoPath: String = ""
    private val TAKE_PHOTO_REQUEST = 101
    private var id_repartidor = 0
    private var orden = ""

    private lateinit var database: DatabaseReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_form_status, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
        id_repartidor = SessionPrefs.getInstance(activity!!).getIdUsuario()

        events()

    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun events() {

        btnASRegresar.setOnClickListener { irAtras() }
        btnActualizar.setOnClickListener {
            if(cbreceta_medica.isChecked){
                  foto_receta()
            }
            else{
                 mPresenter?.actualizar()
            }

        }
    }

    fun showFormScore(){
        val dialog = WADialogForm.Builder(context!!)
            .succes()
            .setTitle("El pedido fue entregado")
            .setMessage("Califica el servicio")
            .setButtonPositive("Enviar", object : WADialogForm.OnClickListener {
                override fun onClick() {
                    getScore(dialogForm!!)

                }
            }).build()
        dialog.show()
        dialogForm = dialog
    }

    fun getScore(dialogForm : WADialogForm) {
        score = dialogForm.getScore()
        scorem = dialogForm.getScoreMessage()
        mPresenter?.actualizarComentario(scorem, score)
    }

    override fun irAtras() {
        (context!! as MainActivity).onBackPressed()
    }

    override fun muestraErrorEnConexion() {
        val dialog = WADialog.Builder(context!!)
             .error()
             .setTitle("Error")
             .setMessage(R.string.msg_sin_conexion)
             .setButtonPositive("Ok", object : WADialog.OnClickListener {
                 override fun onClick() {
                 }
             }).build()
         dialog.show()
    }

    override fun muestraErrorDePeticion(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestrarErrorAlCargar(err: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun showPedido(pedido: Pedido) {
        (context!! as MainActivity).stopOrden()
        var url = "repartidores/"+id_repartidor+"/"+orden
        database.child(url).removeValue()
        showFormScore()
    }

    override fun setOrden(pedido: Pedido) {
        orden = pedido.orden!!
        mensaje.text = "EL STATUS DEL PEDIDO SE ACTUALIZARA A : PEDIDO ENTREGADO"

        if(pedido.receta_requerida != null && pedido.receta_requerida !="" && pedido.receta_requerida == "on"){
            cbreceta_medica.isChecked = true
            cbreceta_medica.isEnabled = false
            mensaje.text = "EL STATUS DEL PEDIDO SE ACTUALIZARA A : PEDIDO ENTREGADO (FOTO OBLIGATORIA DE LA RECETA PARA ESTE PEDIDO)"
        }

    }

    override fun showCalificacion(pedido: Pedido) {
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage("Pedido Finalizado")
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                    (context!! as MainActivity).mostrarFragment(PedidosFragment.getInstance())
                }
            }).build()
        dialog.show()
    }

    override fun setPresenter(presenter: ActualizarContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = ActualizarPedidoFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }

    fun foto_receta(){

        /*
        *  val dialog = WADialog.Builder(context!!)
                            .error()
                            .setTitle("Error")
                            .setMessage("Permiso denegado para abrir la camara")
                            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                                override fun onClick() {
                                }
                            }).build()
                        dialog.show()
                        */

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            launchCamera()
        } else {
            Dexter.withActivity(activity)
                .withPermissions(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA

                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
                            launchCamera()
                        }

                        // check for permanent denial of any permission
                        if (report!!.isAnyPermissionPermanentlyDenied()) {
                            val dialog = WADialog.Builder(context!!)
                                .error()
                                .setTitle("Error")
                                .setMessage("Permiso denegado para abrir la camara")
                                .setButtonPositive("Ok", object : WADialog.OnClickListener {
                                    override fun onClick() {
                                    }
                                }).build()
                            dialog.show()                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                        token!!.continuePermissionRequest()
                    }


                }).withErrorListener {

                }.check()
        }


    }

    /* Lanzador de intent camara */
    private fun launchCamera() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        val fileUri = activity!!.contentResolver
            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(intent.resolveActivity(activity!!.packageManager) != null) {
            mCurrentPhotoPath = fileUri.toString()
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
        }
    }

    /* RESULT DE INTENT */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == TAKE_PHOTO_REQUEST) {
            processPhotoFromCamera()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    /* PROCESAR FOTOGRAFIA CAPTURADA */
    private fun processPhotoFromCamera() {
        val cursor = activity!!.contentResolver.query(Uri.parse(mCurrentPhotoPath),
            Array(1) { android.provider.MediaStore.Images.ImageColumns.DATA },
            null, null, null)
        cursor.moveToFirst()
        mPhotoPath = cursor.getString(0)
        cursor.close()
        val file = File(mPhotoPath)
        mPresenter?.actualizarImagen(mPhotoPath)
    }
}