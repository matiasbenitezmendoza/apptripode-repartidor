package com.webandando.tripode.ui.tutorial

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.webandando.tripode.MainActivity
import com.webandando.tripode.R
import kotlinx.android.synthetic.main.activity_tutorial.*

class TutorialActivity : AppCompatActivity(), TutorialContract.View {

    private var mPresenter: TutorialContract.Presenter? = null
    private lateinit var mFragmentStatePagerAdapter: FragmentStatePagerAdapter
    private val STEPS = 2
    private var indexPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        mFragmentStatePagerAdapter = TutorialAdapter(supportFragmentManager, STEPS)
        mViewPager.adapter = mFragmentStatePagerAdapter
        mCircleIndicator.setViewPager(mViewPager)
        mFragmentStatePagerAdapter.registerDataSetObserver(mCircleIndicator.dataSetObserver)

        btnNextTutorial.setOnClickListener {
            mostrarSiguientePaso()
            setColorNavigationBar(R.color.pumpkin)
        }
        btnEndTutorial.setOnClickListener { mostrarInicio() }

        btnNextTutorial.typeface = Typeface.createFromAsset(assets, "fonts/opensans_semibold.ttf")
        btnEndTutorial.typeface = Typeface.createFromAsset(assets, "fonts/opensans_semibold.ttf")

        mViewPager.currentItem = indexPage
        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                indexPage = position
                if (indexPage < (STEPS-1)) {
                    btnNextTutorial.visibility = View.VISIBLE
                    btnEndTutorial.visibility = View.GONE
                    setColorNavigationBar(R.color.cerulean)
                } else {
                    btnEndTutorial.visibility = View.VISIBLE
                    btnNextTutorial.visibility = View.GONE
                    setColorNavigationBar(R.color.pumpkin)
                }
            }

        })
    }

    override fun setPresenter(presenter: TutorialContract.Presenter) {
        mPresenter = presenter
    }

    override fun mostrarSiguientePaso() {
        indexPage += 1
        mViewPager.currentItem = indexPage
    }

    private fun setColorNavigationBar(color: Int) {
        containerTutorial.setBackgroundColor(ContextCompat.getColor(this, color))
    }

    override fun mostrarInicio() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}