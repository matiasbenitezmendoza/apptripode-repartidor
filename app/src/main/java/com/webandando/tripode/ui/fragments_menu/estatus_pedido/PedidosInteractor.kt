package com.webandando.tripode.ui.fragments_menu.estatus_pedido

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.pedido.source.PedidoService
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.util.config.Constants
import com.webandando.tripode.util.Fecha
import com.webandando.tripode.util.Network
import java.lang.Exception
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class PedidosInteractor(private val context: Context) {

   private val service: PedidoService = PedidoService.instance(context)



    fun getPedidos(callback: Callback) {
        consultarPedidos(callback)
    }



    private fun consultarPedidos(callback: Callback) {
        service.get(100, 0, object : ResponseListener<ResponseData<ArrayList<Pedido>>> {

            override fun onSuccess(response: ResponseData<ArrayList<Pedido>>) {
                callback.onRequestPedidosSucces(response.data as ArrayList<Pedido>)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }




    /* interface para notificar al presentador cualquier evento */
    interface Callback {

        fun onNetworkConnectionFailed()
        fun onRequestFailed(err: String)
        fun onPedidosSuccess(msg: String)
        fun onPedidosFailed(err: String)
        fun onRequestPedidosSucces(pedido: ArrayList<Pedido>)

    }
}
