package com.webandando.tripode.ui.login

import android.content.Context
import android.os.Build
import android.util.Patterns
import android.text.TextUtils
import android.util.Log
import com.webandando.tripode.BuildConfig
import com.webandando.tripode.R
import com.webandando.tripode.data.login.LoginBody
import com.webandando.tripode.data.login.service.LoginService
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.data.session.SessionPrefs
import com.webandando.tripode.util.config.Constants
import com.webandando.tripode.util.Network
import java.util.regex.Pattern

class LoginInteractor (private val context: Context) {

    private val min = 6
    private val service = LoginService(context)

    fun loginWithEmail(email: String, passwd: String, callback: Callback) {

        if (!isValidEmail(email, callback)) return
        if (!isValidPassword(passwd, callback)) return
        if (!isNetworkAvalible(callback)) return
        singInWithEmail(email, passwd, callback)
    }

    fun loginWithFacebook(callback: Callback) {
        if (!isNetworkAvalible(callback)) return
        Log.d(LoginInteractor::class.java.name, "try login with facebook")
        callback.onAuthSuccess()
    }

    /* verifica que el correo ingresado sea valido*/
    private fun isValidEmail(email: String, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(email)) {
            callback.onEmailError(context.getString(R.string.text_error_correo_vacio))
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onEmailError(context.getString(R.string.text_error_correo))
            return false
        }
        return true
    }

    /* verifica que la contraseña ingresada sea valido*/
    private fun isValidPassword(passwd: String, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(passwd)) {
            callback.onPasswordError(context.getString(R.string.text_passwd_vacio))
            return false
        }

        val patron = Pattern.compile(Constants.patternPasswd)
        if (!patron.matcher(passwd).matches()) {
            callback.onPasswordError(context.getString(R.string.text_passwd_no_valido))
            return false
        }

        if (passwd.length < min) {
            callback.onPasswordError(context.getString(R.string.text_passwd_min))
            return false
        }
        return true
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }

    /* intenta loguearse */
    private fun singInWithEmail(email: String, passwd: String, callback: Callback) {
        Log.d(LoginInteractor::class.java.name, "login with email: $email and pass: $passwd")

        service.post(
            LoginBody(email, passwd, BuildConfig.VERSION_NAME),
            object : ResponseListener<ResponseData<String>> {

            override fun onSuccess(response: ResponseData<String>) {
                SessionPrefs(context).saveToken(response.data!!)
                callback.onAuthSuccess()
            }

            override fun onError(t: Throwable) {
                callback.onAuthError(t.message!!)
            }

        })
    }

    /* interface para notificar al presentador cualquier evento */
    interface Callback {
        fun onEmailError(err: String)
        fun onPasswordError(err: String)
        fun onNetworkConnectionFailed()
        fun onAuthSuccess()
        fun onAuthError(err: String)
    }
}