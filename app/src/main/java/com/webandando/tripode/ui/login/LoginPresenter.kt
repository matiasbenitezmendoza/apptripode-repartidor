package com.webandando.tripode.ui.login

import android.util.Log
import androidx.annotation.NonNull

class LoginPresenter(@NonNull val mLoginView : LoginContract.View,
                     @NonNull val mLoginInteractor: LoginInteractor ) : LoginContract.Presenter, LoginInteractor.Callback {

    init {
        mLoginView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
    }

    override fun intentaLoginPorCorreo(correo: String, passwd: String) {
        mLoginInteractor.loginWithEmail(correo, passwd, this)
    }

    override fun intentaLoginPorFacebook() {
        mLoginInteractor.loginWithFacebook(this)
    }

    /* TODO implementacion de interactor callback */

    override fun onEmailError(err: String) {
        mLoginView.muestraErrorEnCorreo(err)
    }

    override fun onPasswordError(err: String) {
        mLoginView.muestraErrorEnPasswd(err)
    }

    override fun onNetworkConnectionFailed() {
        mLoginView.muestrarErrorEnNetwork()
    }

    override fun onAuthSuccess() {
        mLoginView.muestraTutorial()

        Log.d("TAG", "tutorial")


    }

    override fun onAuthError(err: String) {
        mLoginView.muestrarErrorDeLogin(err)
    }

}