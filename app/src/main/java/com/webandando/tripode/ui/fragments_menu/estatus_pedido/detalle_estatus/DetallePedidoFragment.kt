package com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus
import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.util.lib.FragmentChildNotFab
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido.UbicacionPedidoFragment
import com.webandando.tripode.MainActivity
import com.webandando.tripode.data.mapa.Mapa
import com.webandando.tripode.data.session.SessionPrefs
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido.ActualizarContract
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido.ActualizarPedidoFragment
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido.ActualizarPedidoInteractor
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido.ActualizarPedidoPresenter
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido.UbicacionPedidoContract
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido.UbicacionPedidoPresenter
import kotlinx.android.synthetic.main.fragment_detalle_pedido.*
import com.webandando.tripode.util.config.Constants
import com.webandando.walibray.dialog.WADialog
import java.util.*
import kotlin.concurrent.schedule


class DetallePedidoFragment : FragmentChildNotFab(), DetalleContract.View {

    private lateinit var pedido: Pedido
    private var mPresenter: DetalleContract.Presenter? = null
    private lateinit var vw: View


    private lateinit var database: DatabaseReference
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var id_repartidor = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_detalle_pedido, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)
        events()
        mPresenter?.start()
    }

    private fun events() {
        id_repartidor = SessionPrefs.getInstance(activity!!).getIdUsuario()

        btnRegresar.setOnClickListener { irAtras() }

        btnUbicacion.setOnClickListener {
            Log.d("TAG", "RESIDENCIAL"+ pedido.residencial)



            UbicacionPedidoPresenter(
                UbicacionPedidoFragment.instance() as UbicacionPedidoContract.View, this.pedido, context!!)
            fragmentTransaction(UbicacionPedidoFragment.instance())
        }

        btnActualizarStatus.setOnClickListener {
            val interactor = ActualizarPedidoInteractor(context!!)
            ActualizarPedidoPresenter(
                ActualizarPedidoFragment.instance() as ActualizarContract.View, interactor, this.pedido)
            fragmentTransaction(ActualizarPedidoFragment.instance())
        }

        btnRuta.setOnClickListener {
            setPermisos()
        }

        //fabEdit.setOnClickListener { irAEditarInformacionUsuario() }
    }

    private fun fragmentTransaction(fragment: Fragment) {
        activity!!
            .supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
            .commit()
    }

    fun setPermisos(){

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mPresenter?.iniciarRuta(pedido)
        } else {
            Dexter.withActivity(this.activity)
                .withPermission(
                    Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        mPresenter?.iniciarRuta(pedido)
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token!!.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        val dialog = WADialog.Builder(context!!)
                            .error()
                            .setTitle("Error")
                            .setMessage("Permiso denegado para mandar la ubicacion")
                            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                                override fun onClick() {
                                }
                            }).build()
                        dialog.show()
                    }


                }).withErrorListener {

                }.check()
        }


    }

    @SuppressLint("MissingPermission")
    fun subirOrden(){
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                if(location != null){
                    var latlng  = java.util.HashMap<String, Any>()
                    latlng.put("latitud",location.latitude)
                    latlng.put("longitud", location.longitude)
                    var url = "repartidores/"+id_repartidor+"/"+pedido.orden
                    database.child(url).setValue(latlng)
                }
            }
        btnUbicacion.visibility = View.VISIBLE
        btnActualizarStatus.visibility = View.VISIBLE
        btnRuta.visibility = View.GONE
        (context!! as MainActivity).initOrden(pedido.orden!!)
    }

    override fun irAtras() {
        (context!! as MainActivity).onBackPressed()
    }

    override fun showPedido(p: Pedido) {
        this.pedido = p
        tvdpCliente.text = pedido.cliente
        tvdpOrden.text = "#"+pedido.orden
        tvdpFecha.text = pedido.fecha
        tvdpHora.text = pedido.hora
        tvdpTelefono.text = pedido.telefono
        tvdpTipoPago.text = "Tarjeta"
        tvdpStatusPago.text = "Pagado"
        tvdpDireccion.text = pedido.direccion()
        tvdpStatusPedido.text = "Entregado"

        if (pedido != null) {

            if(pedido.status_pedido != 2){
                btnUbicacion.visibility = View.GONE
                btnActualizarStatus.visibility = View.GONE
                btnRuta.visibility = View.VISIBLE
            }else{
                var ruta_url = "repartidores/"+id_repartidor+"/"+pedido.orden
                val postListener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        var punto: Mapa? = dataSnapshot.getValue(Mapa::class.java)
                        if(punto != null){
                            btnUbicacion.visibility = View.VISIBLE
                            btnActualizarStatus.visibility = View.VISIBLE
                            btnRuta.visibility = View.GONE
                        }else{
                            btnUbicacion.visibility = View.GONE
                            btnActualizarStatus.visibility = View.GONE
                            btnRuta.visibility = View.VISIBLE
                        }
                    }
                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                }
                database.child(ruta_url).addListenerForSingleValueEvent(postListener)

            }

            if(pedido.status_pedido!! == 1){
                tvdpStatusPedido.text = "Pedido Recibido"
            }

            if(pedido.status_pedido!! == 2){
                tvdpStatusPedido.text = "Saliendo a tu Direccion"
            }

            if(pedido.status_pago == 2){
                tvdpStatusPago.text = "Pendiente"
            }

            if(pedido.tipo_pago == 2){
                tvdpTipoPago.text = "Tarjeta en Domicilio"
            }

            if(pedido.tipo_pago == 3){
                tvdpTipoPago.text = "Efectivo"
            }


        }

    }

    override fun showNetworkError() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage("Revise su conexion de internet")
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()    }

    override fun setPresenter(presenter: DetalleContract.Presenter) {
        mPresenter = presenter
    }

    override fun showErrorActualizado(mensaje: String) {

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(mensaje)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun showActualizado(pedido: Pedido) {
        this.pedido.status_pedido = 2
        subirOrden()
    }

    companion object {
        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DetallePedidoFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }
}