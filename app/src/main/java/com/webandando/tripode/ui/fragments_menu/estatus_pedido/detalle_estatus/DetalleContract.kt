package com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus
import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView
import com.webandando.tripode.data.pedido.Pedido

interface DetalleContract {

    interface View : BaseView<Presenter> {
        fun showPedido(pedido: Pedido)
        fun showNetworkError()
        fun irAtras()
        fun showActualizado(pedido: Pedido)
        fun showErrorActualizado(error: String)

    }

    interface Presenter : BasePresenter {
        fun irUbicacion()
        fun irActualizar()
        fun iniciarRuta(pedido: Pedido)

    }
}
