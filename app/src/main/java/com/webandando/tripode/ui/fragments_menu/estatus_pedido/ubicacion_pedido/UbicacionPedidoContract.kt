package com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido

import com.webandando.tripode.BasePresenter
import com.webandando.tripode.BaseView
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.residencial.Residencial

interface UbicacionPedidoContract {

    interface View : BaseView<Presenter> {
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)
        fun mostratMapa(residencial: Residencial, orden: Pedido)

    }

    interface Presenter : BasePresenter {
        fun getResidencial(residencial: String)
    }

}