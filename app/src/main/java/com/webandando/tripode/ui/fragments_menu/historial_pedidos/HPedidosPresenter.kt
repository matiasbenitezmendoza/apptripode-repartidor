package com.webandando.tripode.ui.fragments_menu.historial_pedidos

import android.util.Log
import com.webandando.tripode.data.pedido.Pedido



class HPedidosPresenter(val mPedidoView : HPedidosContract.View,
                        val mPedidoInteractor: HPedidosInteractor) : HPedidosContract.Presenter, HPedidosInteractor.Callback {

    init {
        mPedidoView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
       // mPedidoView.muestraMensajesSuccess("desde start")

        Log.d("TAG", "init pedidos")
        getPedidos(10, 0)


    }

    override fun getPedidos(limit: Int, offset: Int) {

        mPedidoInteractor.getPedidos(limit, offset, this)

    }
    override fun goDetallePedido(pedido: Pedido) {

        // mPedidoInteractor.singupUser(user, this)
    }


    override fun onNetworkConnectionFailed() {
        mPedidoView.muestraErrorEnConexion()
    }


    override fun onPedidosSuccess(msg: String) {
        mPedidoView.muestraMensajesSuccess(msg)
    }

    override fun onPedidosFailed(err: String) {
        mPedidoView.muestraErrorDePeticion(err)
    }


    override fun onRequestFailed(err: String) {
        mPedidoView.muestrarErrorAlCargar(err)
    }

    override fun onRequestPedidosSucces(pedidos: ArrayList<Pedido>) {
        mPedidoView.agregarPedidosFilterList(pedidos)
    }



}