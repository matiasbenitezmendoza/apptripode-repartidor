package com.webandando.tripode.ui.fragments_menu.perfil

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.webandando.tripode.R
import com.webandando.tripode.data.session.SessionPrefs
import com.webandando.tripode.ui.login.LoginActivity
import com.webandando.tripode.util.config.Constants
import com.webandando.tripode.util.lib.FragmentParentNotFab
import kotlinx.android.synthetic.main.fragment_opcion_perfil.*
import java.net.URL

class MenuPerfilFragment : FragmentParentNotFab(), MenuPerfilContract.View {

    private var mPresenter: MenuPerfilContract.Presenter? = null
    private lateinit var vw: View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val interactor = MenuPerfilInteractor(context!!)
        MenuPerfilPresenter(this, interactor)

        mPresenter?.start()
        events()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw =  inflater.inflate(R.layout.fragment_opcion_perfil, container, false)
        return vw
    }

    private fun events() {
        btnCerrarSesion.setOnClickListener {
            irALogin()
        }
    }

    private class DownLoadImageTask( val imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg urls: String): Bitmap? {
            val urlOfImage = urls[0]
            return try {
                val inputStream = URL(urlOfImage).openStream()
                BitmapFactory.decodeStream(inputStream)
            } catch (e: Exception) { // Catch the download exception
                e.printStackTrace()
                null
            }
        }
        override fun onPostExecute(result: Bitmap?) {
            if(result!=null){
                imageView.setImageBitmap(result)
            }else{
            }
        }
    }

    /*implementacion contract.view*/
    @SuppressLint("SetTextI18n")
    override fun muestraInformacionUsuario() {
        val usuario = SessionPrefs(context!!).currentUsu()
        tvNameUser.text = "${usuario.nombre} ${usuario.apellidoPaterno} ${usuario.apellidoMaterno}"
        tvEmailUser.text = "${usuario.correo}"
        tvTelefono.text = "${usuario.telefono}"

        if(usuario.imagen != null && usuario.imagen != "") {
            DownLoadImageTask(ivfotoperfil)
                .execute("http://www.farmaciastripode.com/admin/imgs/repartidores/${usuario.imagen}")
        }
    }



    override fun irALogin() {
        SessionPrefs(context!!).logOut()
        val intent = Intent(context, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    override fun setPresenter(presenter: MenuPerfilContract.Presenter) {
        this.mPresenter = presenter
    }

    private fun fragmentTransaction(fragment: Fragment) {
        activity!!
            .supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
            .commit()
    }


    companion object {

        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = MenuPerfilFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }

    }
}