package com.webandando.tripode.ui.fragments_menu.inicio


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.walibray.dialog.WADialog
import com.webandando.tripode.util.lib.FragmentParentNotFab
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_inicio.*
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.webandando.tripode.MainActivity
import com.webandando.tripode.data.mapa.Mapa
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleContract
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetallePedidoFragment
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetallePedidoPresenter
import com.webandando.tripode.util.config.Constants
import kotlinx.android.synthetic.main.fragment_inicio.*
import java.util.*
import kotlin.collections.ArrayList


class InicioFragment :  FragmentParentNotFab(), InicioContract.View {



    private var mPresenter: InicioContract.Presenter? = null
    private lateinit var cntx: Context

    private val mPedidos: ArrayList<Pedido> = ArrayList()
    private lateinit var mAdapter: InicioAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_inicio, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val interactor = InicioInteractor(context!!)
        InicioPresenter(this, interactor)
        initAdapter()

    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }


    private fun goDetallePedido(pedido: Pedido) {
        DetallePedidoPresenter(
            DetallePedidoFragment.instance() as DetalleContract.View, pedido, context!!)
        fragmentTransaction(DetallePedidoFragment.instance())
    }

    private fun fragmentTransaction(fragment: Fragment) {
        activity!!
            .supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
            .commit()
    }



    private fun initAdapter() {
        mAdapter = InicioAdapter(mPedidos, object : InicioAdapter.ListenerView {
            override fun onClick(item: Pedido) {
                goDetallePedido(item)
            }
        })
        mRecyclerView.layoutManager = LinearLayoutManager(context!!)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mAdapter

    }


    override fun muestraProgress(show: Boolean) {
    }

    override fun muestraErrorEnConexion() {
       val dialog = WADialog.Builder(context!!)
             .error()
             .setTitle("Error")
             .setMessage(R.string.msg_sin_conexion)
             .setButtonPositive("Ok", object : WADialog.OnClickListener {
                 override fun onClick() {
                 }
             }).build()
         dialog.show()
    }

    override fun muestraErrorDePeticion(error: String) {
         /*val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()*/
    }

    override fun muestrarErrorAlCargar(err: String) {
        /* val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()*/

    }

    override fun muestraMensajesSuccess(mensaje: String) {
    }

    override fun agregarPedidosFilterList(pedidos: ArrayList<Pedido>) {
        if(pedidos.size > 0){
            actionInfo.visibility = View.GONE
            titulo.visibility = View.VISIBLE
        }else{
            actionInfo.visibility = View.VISIBLE
            titulo.visibility = View.GONE
        }
        mPedidos.clear()
        mPedidos.addAll(pedidos)
        mAdapter.notifyDataSetChanged()
    }

    override fun setPresenter(presenter: InicioContract.Presenter) {
        mPresenter = presenter
    }






    companion object {

        private var fragment : Fragment? = null

        @Synchronized fun getInstance() : Fragment {
            if (fragment == null) {
                fragment = InicioFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }

    }
}