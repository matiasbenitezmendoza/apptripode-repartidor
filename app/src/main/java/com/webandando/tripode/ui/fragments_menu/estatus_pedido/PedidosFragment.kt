package com.webandando.tripode.ui.fragments_menu.estatus_pedido


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.walibray.dialog.WADialog
import com.webandando.tripode.util.lib.FragmentParentNotFab
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_inicio.*
import android.util.Log


import androidx.fragment.app.FragmentTransaction
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleContract
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetallePedidoFragment
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetallePedidoPresenter
import com.webandando.tripode.util.config.Constants
import kotlinx.android.synthetic.main.fragment_inicio.*


class PedidosFragment :  FragmentParentNotFab(), PedidosContract.View {



    private var mPresenter: PedidosContract.Presenter? = null
    private lateinit var cntx: Context

    private val mPedidos: ArrayList<Pedido> = ArrayList()
    private lateinit var mAdapter: PedidosAdapter



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pedido, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val interactor = PedidosInteractor(context!!)
        PedidosPresenter(this, interactor)
        setEvents()
        initAdapter()

    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }


    private fun goDetallePedido(pedido: Pedido) {
        DetallePedidoPresenter(
            DetallePedidoFragment.instance() as DetalleContract.View, pedido, context!!)
        fragmentTransaction(DetallePedidoFragment.instance())
    }

    private fun fragmentTransaction(fragment: Fragment) {
        activity!!
            .supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
            .commit()
    }



    private fun initAdapter() {
        mAdapter = PedidosAdapter(mPedidos, object : PedidosAdapter.ListenerView {
            override fun onClick(item: Pedido) {
                goDetallePedido(item)
            }
        })
        mRecyclerView.layoutManager = LinearLayoutManager(context!!)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mAdapter

        Log.d("TAG", "init array")

    }

    /* eventos de botonos e inputs de la interfaz*/
    private fun setEvents() {

        Log.d("TAG", "init")
        Log.d("TAG", "eventos")
    }



    override fun muestraProgress(show: Boolean) {
    }

    override fun muestraErrorEnConexion() {
        Log.d("TAG", "mensaje error conexion")

        /* val dialog = WADialog.Builder(context!!)
             .error()
             .setTitle("Error")
             .setMessage(R.string.msg_sin_conexion)
             .setButtonPositive("Ok", object : WADialog.OnClickListener {
                 override fun onClick() {
                 }
             }).build()
         dialog.show()*/
    }

    override fun muestraErrorDePeticion(error: String) {
        Log.d("TAG", "mensaje error pet")

        /* val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()*/
    }

    override fun muestrarErrorAlCargar(err: String) {
        Log.d("TAG", "mensaje error cargar")

         val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }



    override fun muestraMensajesSuccess(mensaje: String) {
        Log.d("TAG", "mensaje succes")

        /*val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage(mensaje)
            .setButtonPositive("Ir a iniciar sesión", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()*/
    }

    override fun agregarPedidosFilterList(pedidos: ArrayList<Pedido>) {
        mPedidos.clear()
        mPedidos.addAll(pedidos)
        mAdapter.notifyDataSetChanged()


        Log.d("TAG", "add pedidos")

        //chipsInput.filterableList = mPedidos
    }

    override fun setPresenter(presenter: PedidosContract.Presenter) {
        mPresenter = presenter
    }






    companion object {

        private var fragment : Fragment? = null

        @Synchronized fun getInstance() : Fragment {
            if (fragment == null) {
                fragment = PedidosFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }

    }
}