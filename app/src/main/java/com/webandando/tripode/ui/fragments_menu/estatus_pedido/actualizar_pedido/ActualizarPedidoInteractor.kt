package com.webandando.tripode.ui.fragments_menu.estatus_pedido.actualizar_pedido

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.data.pedido.source.PedidoService
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.util.config.Constants
import com.webandando.tripode.util.Fecha
import com.webandando.tripode.util.Network
import java.lang.Exception
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class ActualizarPedidoInteractor(private val context: Context) {

    private val service: PedidoService = PedidoService.instance(context)



    fun putPedido(pedido: Pedido, callback: Callback) {
        actualizarPedido(pedido, callback)
    }

    fun putPedidoImagen(imagen: String, id: Int, callback: Callback) {
        actualizarPedidoImagen(imagen, id, callback)
    }

    fun putComentarios(pedido: Pedido, callback: Callback) {
        actualizarPedidoComentarios(pedido, callback)
    }

    private fun actualizarPedidoImagen(imagen: String, id: Int, callback: Callback) {
        Log.d("TAG", "put pedidos imagen")

        service.put_imagen(imagen, id, object : ResponseListener<ResponseData<Pedido>> {

            override fun onSuccess(response: ResponseData<Pedido>) {
                callback.onRequestPedidoSucces(response.data as Pedido)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })


    }

    private fun actualizarPedido(pedido: Pedido, callback: Callback) {
        Log.d("TAG", "put pedidos")

        service.put(pedido, object : ResponseListener<ResponseData<Pedido>> {

            override fun onSuccess(response: ResponseData<Pedido>) {
                callback.onRequestPedidoSucces(response.data as Pedido)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })

        /*
        service.get(10, 0, object : ResponseListener<ResponseData<ArrayList<Pedido>>> {

            override fun onSuccess(response: ResponseData<ArrayList<Pedido>>) {
                callback.onRequestPedidosSucces(response.data as ArrayList<Pedido>)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })*/
    }

    private fun actualizarPedidoComentarios(pedido: Pedido, callback: Callback) {

        service.putComentarios(pedido, object : ResponseListener<ResponseData<Pedido>> {

            override fun onSuccess(response: ResponseData<Pedido>) {
                callback.onRequestScoreSucces(response.data as Pedido)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })

        /*
        service.get(10, 0, object : ResponseListener<ResponseData<ArrayList<Pedido>>> {

            override fun onSuccess(response: ResponseData<ArrayList<Pedido>>) {
                callback.onRequestPedidosSucces(response.data as ArrayList<Pedido>)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })*/
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }


    /* interface para notificar al presentador cualquier evento */
    interface Callback {

        fun onNetworkConnectionFailed()
        fun onRequestFailed(err: String)
        fun onRequestPedidoSucces(pedido: Pedido)
        fun onRequestScoreSucces(pedido: Pedido)


    }
}
