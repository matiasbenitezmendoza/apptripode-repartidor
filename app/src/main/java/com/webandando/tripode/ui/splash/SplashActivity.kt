package com.webandando.tripode.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.webandando.tripode.MainActivity
import com.webandando.tripode.R
import com.webandando.tripode.data.session.SessionPrefs
import com.webandando.tripode.ui.login.LoginActivity
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        time()
    }

    private fun time() {
        val task = object : TimerTask() {
            override fun run() {

                if(SessionPrefs(this@SplashActivity).isLogin()) {
                    val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                  //  val mainIntent = Intent(this@SplashActivity, FondoActivity::class.java)
                    startActivity(mainIntent)
                    finish()
                } else {
                    val mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                    startActivity(mainIntent)
                    finish()
                }

            }
        }

        val timer = Timer()
        timer.schedule(task, 3000)
    }


}