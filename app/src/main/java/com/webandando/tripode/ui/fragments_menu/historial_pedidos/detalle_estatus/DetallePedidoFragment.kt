package com.webandando.tripode.ui.fragments_menu.historial_pedidos.detalle_estatus
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.webandando.tripode.R
import com.webandando.tripode.data.pedido.Pedido
import com.webandando.tripode.util.lib.FragmentChildNotFab
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import com.webandando.tripode.MainActivity

import kotlinx.android.synthetic.main.fragment_detalle_pedido.*
import com.webandando.tripode.util.config.Constants
import kotlinx.android.synthetic.main.fragment_detalle_hpedido.*
import kotlinx.android.synthetic.main.fragment_detalle_pedido.btnRegresar


class DetallePedidoFragment : FragmentChildNotFab(), DetalleContract.View {

    private lateinit var pedido: Pedido
    private var mPresenter: DetalleContract.Presenter? = null
    private lateinit var vw: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_detalle_hpedido, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        events()
        mPresenter?.start()
    }

    private fun events() {
        btnRegresar.setOnClickListener { irAtras() }


        //fabEdit.setOnClickListener { irAEditarInformacionUsuario() }
    }

    private fun fragmentTransaction(fragment: Fragment) {
        activity!!
            .supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
            .commit()
    }

    override fun irAtras() {
        (context!! as MainActivity).onBackPressed()
    }

    override fun showPedido(pedido: Pedido) {
        this.pedido = pedido
        tvdhpCliente.text = pedido.cliente
        tvdhpOrden.text = "#"+pedido.orden
        tvdhpFecha.text = pedido.fecha
        tvdhpHora.text = pedido.hora
        tvdhpTipoPago.text = "Tarjeta"
        tvdhpTelefono.text = pedido.telefono
        tvdhpStatusPago.text = "Pagado"
        tvdhpDireccion.text = pedido.direccion()
        tvdhpStatusPedido.text = "Entregado"

        if (pedido != null) {

            if(pedido.status_pedido!! < 3){
                tvdhpStatusPedido.text = "Pendiente"
            }

            if(pedido.status_pago == 2){
                tvdhpStatusPago.text = "Pendiente"
            }

            if(pedido.tipo_pago == 2){
                tvdhpTipoPago.text = "Tarjeta en Domicilio"
            }

            if(pedido.tipo_pago == 3){
                tvdhpTipoPago.text = "Efectivo"
            }


        }



    }



    override fun showNetworkError() {
        Snackbar.make(mainContainer, "Sin conexion a internet", Snackbar.LENGTH_LONG).show()
    }


    override fun setPresenter(presenter: DetalleContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DetallePedidoFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }
}