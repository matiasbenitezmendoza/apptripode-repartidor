package com.webandando.tripode.data.pedido.source

import com.webandando.tripode.data.pedido.Pedido
import android.content.Context
import android.util.Log
import com.webandando.tripode.R
import com.webandando.tripode.data.detallepedido.Detallepedido
import com.webandando.tripode.data.objects.RetrofitBuilder
import com.webandando.tripode.data.objects.WADialogProgressBuilder
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.util.config.ConvertBodyError
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File

class PedidoService(var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: PedidoApi

    init {
        api = retrofit.create(PedidoApi::class.java)
    }

    fun get(limit: Int, offset: Int, onResponse: ResponseListener<ResponseData<ArrayList<Pedido>>>) {
        waprogress.show()
        api.get(limit, offset).enqueue( object : Callback<ResponseData<ArrayList<Pedido>>>{

            override fun onFailure(call: Call<ResponseData<ArrayList<Pedido>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Pedido>>>,
                response: Response<ResponseData<ArrayList<Pedido>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Pedido>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Pedido>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Pedido>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun getHistorial(limit: Int, offset: Int, onResponse: ResponseListener<ResponseData<ArrayList<Pedido>>>) {
        waprogress.show()
        api.getHistorial(limit, offset).enqueue( object : Callback<ResponseData<ArrayList<Pedido>>>{

            override fun onFailure(call: Call<ResponseData<ArrayList<Pedido>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Pedido>>>,
                response: Response<ResponseData<ArrayList<Pedido>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Pedido>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Pedido>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Pedido>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun getPedidosRuta(limit: Int, offset: Int, onResponse: ResponseListener<ResponseData<ArrayList<Pedido>>>) {
        waprogress.show()
        api.getPedidosRuta(limit, offset).enqueue( object : Callback<ResponseData<ArrayList<Pedido>>>{

            override fun onFailure(call: Call<ResponseData<ArrayList<Pedido>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Pedido>>>,
                response: Response<ResponseData<ArrayList<Pedido>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Pedido>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Pedido>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Pedido>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun put(pedido: Pedido, onResponse: ResponseListener<ResponseData<Pedido>>) {
        waprogress.show()
        api.put(pedido).enqueue(object : Callback<ResponseData<Pedido>> {

            override fun onFailure(call: Call<ResponseData<Pedido>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Pedido>>, response: Response<ResponseData<Pedido>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Pedido>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun put_imagen( sreceta: String, id: Int, onResponse: ResponseListener<ResponseData<Pedido>>) {
        waprogress.show()
        if (sreceta.isEmpty()) {
            onResponse.onError(Throwable("Sin imagen"))
        }

        var receta : MultipartBody.Part? = null
        if (!sreceta.isEmpty()) {
            receta = try {
                val image = File(sreceta)
                val imageBody: RequestBody = RequestBody.create(MediaType.parse("image/*"), image)
                MultipartBody.Part.createFormData("receta", image.name, imageBody)
            } catch (e : Exception) {
                e.printStackTrace()
                null
            }
        }

        if (receta != null) {

            val rbId= RequestBody.create(MultipartBody.FORM, id.toString())
            Log.d("TAG", "par1: "+receta)
            Log.d("TAG", "par2: "+rbId)

            api.put_imagen(receta, rbId).enqueue(object : Callback<ResponseData<Pedido>> {

                override fun onFailure(call: Call<ResponseData<Pedido>>, t: Throwable) {
                    waprogress.hide()
                    onResponse.onError(t)
                }

                override fun onResponse(call: Call<ResponseData<Pedido>>, response: Response<ResponseData<Pedido>>) {
                    waprogress.hide()
                    if ( response.errorBody() != null ) {
                        when {
                            response.code() in 400..499 -> {
                                val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                                onResponse.onError(Throwable(responseModel?.message))
                            }
                            response.code() in 500..599 -> {
                                val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                                onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                            }
                            else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                        }
                    } else {
                        try {
                            val responseModel = response.body() as ResponseData<Pedido>
                            onResponse.onSuccess(responseModel)
                        } catch (e: Exception) {
                            onResponse.onError(Throwable(e.message))
                        }
                    }
                }
            })


        }   else {
            onResponse.onError(Throwable("No se encontro la imagen"))
        }
    }

    fun putComentarios(pedido: Pedido,onResponse: ResponseListener<ResponseData<Pedido>>) {
        waprogress.show()

        api.putComentarios(pedido).enqueue(object : Callback<ResponseData<Pedido>> {

            override fun onFailure(call: Call<ResponseData<Pedido>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Pedido>>, response: Response<ResponseData<Pedido>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Pedido>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun putRuta(pedido: Pedido,onResponse: ResponseListener<ResponseData<Pedido>>) {
        waprogress.show()

        api.putRuta(pedido).enqueue(object : Callback<ResponseData<Pedido>> {

            override fun onFailure(call: Call<ResponseData<Pedido>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Pedido>>, response: Response<ResponseData<Pedido>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Pedido>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Pedido>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : PedidoService {
            return PedidoService(context)
        }
    }


}