package com.webandando.tripode.data.residencial.source

import android.content.Context
import com.webandando.tripode.R
import com.webandando.tripode.data.objects.RetrofitBuilder
import com.webandando.tripode.data.objects.WADialogProgressBuilder

import com.webandando.tripode.data.residencial.Residencial
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ResidencialService(var context: Context) {
    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: ResidencialApi

    init {
        api = retrofit.create(ResidencialApi::class.java)
    }

    fun get(residencial: String, onResponse: ResponseListener<ResponseData<Residencial>>) {
        waprogress.show()
        api.get(residencial).enqueue( object : Callback<ResponseData<Residencial>> {

            override fun onFailure(call: Call<ResponseData<Residencial>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<Residencial>>,
                response: Response<ResponseData<Residencial>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Residencial>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Residencial>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Residencial>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : ResidencialService {
            return ResidencialService(context)
        }
    }


}

