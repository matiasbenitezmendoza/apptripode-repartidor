package com.webandando.tripode.data.residencial

import com.google.gson.annotations.SerializedName



class Residencial (
    @SerializedName("id") var id: Int? = 0,
    @SerializedName("nombre") var nombre: String = "",
    @SerializedName("latitud") var latitud: Double? = 0.0,
    @SerializedName("longitud") var longitud: Double? = 0.0
) {
    override fun toString(): String {
        return nombre
    }
}


