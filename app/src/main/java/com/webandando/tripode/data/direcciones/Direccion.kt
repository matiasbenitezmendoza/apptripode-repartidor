package com.webandando.tripode.data.direcciones

class Direccion(
    var nombreReferencia: String = "",
    var calle: String = "",
    var noExt: String = "",
    var noInt: String = "",
    var residencial: String = "",
    var codigoPostal: String = "",
    var estado: String = "",
    var colonia: String = "",
    var ciudad: String = "",
    var referencias: String = "",
    var selected: Boolean = false
) {


    fun detalle() : String {
        var noint = ""
        if (noInt.isNotEmpty())
            noint = "int $noInt"

        return  "$calle $noExt $noint\nCol. $colonia\n"+
                "Residencial $residencial C.P. $codigoPostal\n"+
                "$estado\n$referencias"
    }
}