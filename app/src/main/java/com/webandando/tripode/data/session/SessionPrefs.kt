package com.webandando.tripode.data.session

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.webandando.tripode.BuildConfig
import com.webandando.tripode.data.usuario.Usuario

class SessionPrefs (var context: Context) {

    private var mPrefs: SharedPreferences? = null

    init {
        mPrefs = context.applicationContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
    }

    fun saveToken(token: String) {
        val editor = mPrefs?.edit()
        editor?.putString(USER_TOKEN, token)
        editor?.apply()
    }

    fun isLogin() : Boolean {
        val token = getToken()
        return !TextUtils.isEmpty(token)
    }

    fun currentVersion() : Boolean {
        val usu = currentUsu()
        if (usu.version != null) {
            return usu.version == BuildConfig.VERSION_NAME
        }
        return false
    }

    fun getToken() : String? {
        return mPrefs?.getString(USER_TOKEN, "")
    }

    fun logOut() {
        val editor = mPrefs?.edit()
        editor?.putString(USER_TOKEN, null)
        editor?.apply()
    }

    fun currentUsu() : Usuario {
        val currentUser = Usuario()
        if (isLogin()) {
            currentUser.setValuesFromToken(getToken() ?: "")
        }
        return currentUser
    }


    fun getIdUsuario() : Int {
        val usu = currentUsu()
        return usu.idRepartidor!!
    }


    fun saveOrden(id: String) {
        val editor = mPrefs?.edit()
        editor?.putString(ORDEN_RUTA, id)
        editor?.apply()
    }

    fun getOrden() : String {
        return mPrefs?.getString(ORDEN_RUTA, "") ?: ""
    }


    companion object {
        const val USER_PREFS = "user_prefs_tripode"
        const val USER_TOKEN = "token_benavides"
        const val ORDEN_RUTA = "orden"

        @SuppressLint("StaticFieldLeak")
        fun getInstance(context: Context) : SessionPrefs {
            return SessionPrefs(context)
        }
    }
}