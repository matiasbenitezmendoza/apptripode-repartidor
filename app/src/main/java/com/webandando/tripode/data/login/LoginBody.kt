package com.webandando.tripode.data.login

import com.google.gson.annotations.SerializedName

class LoginBody(
    @SerializedName("user") var user: String?,
    @SerializedName("pass") var pass: String?,
    @SerializedName("version") var version: String?
)