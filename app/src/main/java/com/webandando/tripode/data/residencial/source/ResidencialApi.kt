package com.webandando.tripode.data.residencial.source

import com.webandando.tripode.data.residencial.Residencial
import com.webandando.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ResidencialApi {

    @GET("residencias/ubicacion")
    fun get(@Query("residencial") residencial: String): Call<ResponseData<Residencial>>

}
