package com.webandando.tripode.data.objects

import android.content.Context
import com.webandando.tripode.BuildConfig
import com.webandando.tripode.R
import com.webandando.tripode.data.service.TokenInterceptor
import com.webandando.tripode.data.session.SessionPrefs
import com.webandando.tripode.util.config.Constants
import com.webandando.walibray.dialog.WADialogProgress
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object BuildInterceptor {

    fun getInterceptor(context: Context) : OkHttpClient{
        val token = SessionPrefs.getInstance(context).getToken()

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
            .connectTimeout(40, TimeUnit.SECONDS)
            .addInterceptor(TokenInterceptor(token ?: ""))
            .addInterceptor(loggingInterceptor)
            .readTimeout(40, TimeUnit.SECONDS)
            .build()
    }
}

object RetrofitBuilder {

    fun getRetrofit(context: Context) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.URL_HOTS)
            .addConverterFactory(GsonConverterFactory.create())
            .client(BuildInterceptor.getInterceptor(context))
            .build()
    }
}

object WADialogProgressBuilder {

    fun getProgress(context: Context) : WADialogProgress {
        return WADialogProgress.Builder(context)
            .setDrawable(R.drawable.isotipo)
            .build()
    }

}

