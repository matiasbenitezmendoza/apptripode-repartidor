package com.webandando.tripode.data.login.service

import com.webandando.tripode.data.login.LoginBody
import com.webandando.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("loginrepartidores/login")
    fun login(@Body loginBody: LoginBody) : Call<ResponseData<String>>

}