package com.webandando.tripode.data.login.service

import android.content.Context
import com.webandando.tripode.R
import com.webandando.tripode.data.login.LoginBody
import com.webandando.tripode.data.objects.RetrofitBuilder
import com.webandando.tripode.data.objects.WADialogProgressBuilder
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class LoginService (var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: LoginApi = retrofit.create(LoginApi::class.java)

    fun post( loginBody: LoginBody, onResponse: ResponseListener<ResponseData<String>>) {
        waprogress.show()
        api.login(loginBody).enqueue( object : Callback<ResponseData<String>>{

            override fun onFailure(call: Call<ResponseData<String>>, t: Throwable) {
                waprogress.show()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<String>>, response: Response<ResponseData<String>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<String>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<String>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<String>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }
}