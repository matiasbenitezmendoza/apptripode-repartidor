package com.webandando.tripode.data.direcciones.source

import android.content.Context
import com.webandando.tripode.R
import com.webandando.tripode.data.direcciones.DireccionPostal
import com.webandando.tripode.data.objects.RetrofitBuilder
import com.webandando.tripode.data.objects.WADialogProgressBuilder
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class DireccionService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: DireccionApi

    init {
        api = retrofit.create(DireccionApi::class.java)
    }


    fun getDireccionPostal(codigoPostal: String, onResponse: ResponseListener<ResponseData<DireccionPostal>>) {
        waprogress.show()
        api.getDireccionPostal(codigoPostal).enqueue( object : Callback<ResponseData<DireccionPostal>>{
            override fun onFailure(call: Call<ResponseData<DireccionPostal>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<DireccionPostal>>,
                response: Response<ResponseData<DireccionPostal>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<DireccionPostal>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<DireccionPostal>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<DireccionPostal>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }

            }
        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : DireccionService {
            return DireccionService(context)
        }

    }
}