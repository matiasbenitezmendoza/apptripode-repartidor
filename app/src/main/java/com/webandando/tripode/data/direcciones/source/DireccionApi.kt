package com.webandando.tripode.data.direcciones.source

import com.webandando.tripode.data.direcciones.DireccionPostal
import com.webandando.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface DireccionApi {

    @GET("direcciones/direcciones_cp")
    fun getDireccionPostal(
        @Query("codigo_postal") codigoPostal: String)
            : Call<ResponseData<DireccionPostal>>
}