package com.webandando.tripode.data.service.model

class ResponseData<T>(
    var status: Boolean,
    var message: String,
    var data: T? = null,
    var token: String? = null // token refresh
)