package com.webandando.tripode.data.pedido.source

import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.pedido.Pedido
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface PedidoApi {
    @GET("pedidos/pedidos_repartidor")
    fun get(@Query("limit") limit: Int,   @Query("offset") offset: Int ) : Call<ResponseData<ArrayList<Pedido>>>

    @GET("pedidos/pedidos_repartidor_finalizados")
    fun getHistorial(@Query("limit") limit: Int,   @Query("offset") offset: Int ) : Call<ResponseData<ArrayList<Pedido>>>

    @GET("pedidos/pedidos_repartidor_ruta")
    fun getPedidosRuta(@Query("limit") limit: Int,   @Query("offset") offset: Int ) : Call<ResponseData<ArrayList<Pedido>>>

    @PUT("pedidos/pedidos_repartidor")
    fun put(@Body pedido: Pedido) : Call<ResponseData<Pedido>>

    @PUT("pedidos/calificar_repartidor")
    fun putComentarios( @Body pedido: Pedido ) : Call<ResponseData<Pedido>>

    @PUT("pedidos/iniciar_ruta")
    fun putRuta( @Body pedido: Pedido ) : Call<ResponseData<Pedido>>

    //@Part("tipo") tipo: RequestBody,

    @Multipart
    @POST("pedidos/pedidos_repartidor_imagen")
    fun put_imagen(@Part image: MultipartBody.Part, @Part("id") id: RequestBody): Call<ResponseData<Pedido>>

}