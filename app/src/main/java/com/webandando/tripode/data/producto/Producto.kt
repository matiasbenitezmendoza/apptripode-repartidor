package com.webandando.tripode.data.producto

class Producto (var nombre: String,
                var detalle: String,
                var img: Int,
                var marca: String,
                var itemNum: String,
                var precio: Double,
                var descuento: Double,
                var cantidad: Int = 1)