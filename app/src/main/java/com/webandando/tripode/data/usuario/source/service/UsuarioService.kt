package com.webandando.tripode.data.usuario.source.service

import android.content.Context
import com.webandando.tripode.R
import com.webandando.tripode.data.objects.RetrofitBuilder
import com.webandando.tripode.data.objects.WADialogProgressBuilder
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.data.usuario.Usuario
import com.webandando.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class UsuarioService(var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: UsuarioApi

    init {
        api = retrofit.create(UsuarioApi::class.java)
    }

    fun post(usuario: Usuario, onResponse: ResponseListener<ResponseData<Usuario>>) {
        waprogress.show()
        api.post(usuario).enqueue(object : Callback<ResponseData<Usuario>> {
            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Usuario>>,
                                    response: Response<ResponseData<Usuario>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun get(id: String) {}

    fun put(usuario: Usuario, onResponse: ResponseListener<ResponseData<Usuario>>) {
        waprogress.show()
        api.put(usuario).enqueue(object : Callback<ResponseData<Usuario>> {

            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Usuario>>, response: Response<ResponseData<Usuario>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun sendToken(token: String, onResponse: ResponseListener<ResponseData<Usuario>>) {
        api.sendToken(token).enqueue( object : Callback<ResponseData<Usuario>>{
            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<Usuario>>,
                response: Response<ResponseData<Usuario>>
            ) {
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Usuario>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Usuario>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }


    companion object {

        @Synchronized fun instance(context: Context) : UsuarioService {
            return UsuarioService(context)
        }
    }
}