package com.webandando.tripode.data.usuario

import com.auth0.android.jwt.JWT
import com.google.gson.annotations.SerializedName

class Usuario (
    @SerializedName("nombre") var nombre: String? = "",
    @SerializedName("apellido_paterno") var apellidoPaterno: String? = "",
    @SerializedName("apellido_materno") var apellidoMaterno: String? = "",
    @SerializedName("imagen") var imagen: String? = "",
    @SerializedName("email") var correo: String? = "",
    @SerializedName("password") var password: String? = "",
    @SerializedName("telefono") var telefono: String? = "",
    @SerializedName("id_repartidor") var idRepartidor : Int? = 0,
    @SerializedName("version") var version: String? = null
) {

    fun setValuesFromToken( _token: String ) {
        if (_token.isNotEmpty()) {
            val jwt = JWT(_token)

            idRepartidor = jwt.getClaim(FIELD_ID).asInt()
            nombre = jwt.getClaim(FIELD_NOMBRE).asString()
            imagen = jwt.getClaim(FIELD_IMAGEN).asString()

            correo = jwt.getClaim(FIELD_MAIL).asString()
            apellidoPaterno = jwt.getClaim(FIELD_APELLIDO_PATERNO).asString()
            apellidoMaterno = jwt.getClaim(FIELD_APELLIDO_MATERNO).asString()
            telefono = jwt.getClaim(FIELD_PHONE).asString()
            version = jwt.getClaim(FIELD_VERSION).asString()
        }
    }

    companion object {
        const val FIELD_ID = "id"
        const val FIELD_NOMBRE = "nombre"
        const val FIELD_APELLIDO_PATERNO = "apellido_paterno"
        const val FIELD_APELLIDO_MATERNO = "apellido_materno"
        const val FIELD_MAIL = "email"
        const val FIELD_IMAGEN = "imagen"
        const val FIELD_PHONE = "telefono"
        const val FIELD_VERSION = "version"
    }
}