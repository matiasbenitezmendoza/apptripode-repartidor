package com.webandando.tripode.data.pedido

import com.auth0.android.jwt.JWT
import com.google.gson.annotations.SerializedName

class Pedido(
    @SerializedName("id") var id: Int? = 0,
    @SerializedName("id_repartidor") var idRepartidor: Int? = 0,
    @SerializedName("orden") var orden: String? = "",
    @SerializedName("cliente") var cliente: String? = "",
    @SerializedName("fecha") var fecha: String? = "",
    @SerializedName("hora") var hora: String? = "",
    @SerializedName("status_pedido") var status_pedido: Int? = 0,
    @SerializedName("status_pago") var status_pago: Int? = 0,
    @SerializedName("tipo_pago") var tipo_pago: Int? = 0,
    @SerializedName("calle") var calle: String? = "",
    @SerializedName("noExt") var noExt: String? = "",
    @SerializedName("noInt") var noInt: String = "",
    @SerializedName("residencial") var residencial: String? = "",
    @SerializedName("codigoPostal") var codigoPostal: String? = "",
    @SerializedName("estado") var estado: String? = "",
    @SerializedName("colonia") var colonia: String? = "",
    @SerializedName("ciudad") var ciudad: String? = "",
    @SerializedName("referencias") var referencias: String? = "",
    @SerializedName("tipo_envio") var tipo_envio: String? = "",
    @SerializedName("comentarios_repartidor") var comentarios_repartidor: String? = "",
    @SerializedName("calificacion_repartidor") var calificacion_repartidor: String? = "",
    @SerializedName("telefono") var telefono: String? = "",
    @SerializedName("receta_requerida") var receta_requerida: String? = ""


) {


    fun detalle() : String {

        return  "Orden $orden fecha $fecha Pago $tipo_pago\n"+
                "$calle $noExt $noInt \nCol. $colonia\n"+
                "Residencial $residencial C.P. $codigoPostal\n"+
                "$estado\n$referencias"
    }


    fun direccion() : String {

        return  "$calle $noExt $noInt \nCol. $colonia\n"+
                "Residencial $residencial C.P. $codigoPostal\n"+
                "$estado\n$referencias"
    }



    fun setValuesFromToken( _token: String ) {
        if (_token.isNotEmpty()) {
            val jwt = JWT(_token)

            idRepartidor = jwt.getClaim(FIELD_ID).asInt()
        }
    }

    companion object {
        const val FIELD_ID = "id_repartidor"
    }
}