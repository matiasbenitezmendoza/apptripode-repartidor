package com.webandando.tripode.interfaces

interface Events {

    fun onClick(itemId: Int)
}