package com.webandando.tripode

import androidx.fragment.app.Fragment
import com.webandando.tripode.data.menu.ItemMenu

interface MainContract {

    interface View : BaseView<Presenter> {
        fun openDrawable()
        fun closeDrawable()
        fun mostrarFragment(fragment: Fragment)
        fun clearStack()
        fun fragmentInit(fragment: Fragment)
        fun cargarMenu(listDataHeader: ArrayList<ItemMenu>, listDataChild: HashMap<ItemMenu, List<String>>)
    }

    interface Presenter : BasePresenter {
        fun openOrCloseDrawableMenu(cerrado: Boolean)
        fun onItemMenuSelected(idItemMenu: Int)
        fun sendToken(token: String)

    }
}