package com.webandando.tripode

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.webandando.tripode.interfaces.ToolbarActivity
import com.webandando.tripode.util.Keyboard
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.webandando.tripode.util.config.Constants
import com.webandando.tripode.util.lib.IMainView
import com.webandando.tripode.data.menu.ExpandableListAdapter
import com.webandando.tripode.data.menu.ItemMenu
import com.webandando.tripode.interfaces.Events
import com.webandando.tripode.ui.fragments_menu.inicio.ActualizarService
import android.util.Log
import com.webandando.tripode.data.session.SessionPrefs
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.content.IntentFilter
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.webandando.tripode.ui.fragments_menu.inicio.BootBroadcast
import com.webandando.tripode.ui.fragments_menu.inicio.InicioFragment


class MainActivity : ToolbarActivity(), MainContract.View, IMainView.View {

    private var mPresenter: MainContract.Presenter? = null
    private var mCallback: IMainView.Callback? = null
    private var fragmentSelected: Fragment? = null

    var mMenuAdapter: ExpandableListAdapter? = null
    var listDataHeader: ArrayList<ItemMenu>? = null
    var listDataChild: HashMap<ItemMenu, List<String>>? = null

    private lateinit var prefs: SessionPrefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbarToLoad(toolbar as Toolbar)
        btnMenu.setOnClickListener { onClickMenu() }
        // inicializa el mvp
        val mainInteractor = MainInteractor(applicationContext)
        MainPresenter(this, mainInteractor)
        fab.setOnClickListener { mCallback?.actionFab() }
        initEvents()
        mPresenter?.start()

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                Log.d("TAG", "TOKEN:.:.")
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                val token = task.result?.token
                Log.d("TAG", "TOKEN"+token)

                mPresenter?.sendToken(token!!)
            })

        InicioFragment.destroy()
        val fragment = InicioFragment.getInstance() as InicioFragment
        this.mostrarFragment(fragment)
    }

    fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun initOrden(orden: String){
        prefs = SessionPrefs(this)
        prefs.saveOrden(orden)
        val serviceClass = ActualizarService::class.java
        val intent = Intent(this, serviceClass)
        if (isServiceRunning( ActualizarService::class.java)) {
            stopService(intent)
        }
        startService(intent)
    }


    fun stopOrden(){
        val serviceClass = ActualizarService::class.java
        val intent = Intent(this, serviceClass)
        stopService(intent)
    }

    override fun onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
             closeDrawable()
        } else {

            /*val fragment = supportFragmentManager.findFragmentByTag(TAG_FRAGMENT)
            if ((fragment as FragmentRoot).isRoot) {
                if (fragmentSelected != InicioProductosFragment.instance()) {
                    mostrarFragment(InicioProductosFragment.instance())
                } else {
                    super.onBackPressed()
                }
            } else {
                super.onBackPressed()
            }*/
            super.onBackPressed()
        }
    }

    private fun initEvents() {
        inputSearch.setOnEditorActionListener { textView, actionId, event ->
            var procesado = false
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Snackbar.make(mainContainer, "buscar", Snackbar.LENGTH_LONG).show()
                // Ocultar teclado virtual
                Keyboard.hide(this@MainActivity, textView as TextView)
                procesado = true
                showSearch(false)
            }
            return@setOnEditorActionListener procesado
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (fragmentSelected != null) {
            if (fragmentSelected!!.isAdded) {
                supportFragmentManager.putFragment(outState, "FragmentActual", fragmentSelected!!)
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        fragmentSelected = supportFragmentManager.getFragment(savedInstanceState, "FragmentActual")
        if (fragmentSelected != null)
            mostrarFragment(fragmentSelected!!)
    }

    override fun onClickMenu() {
        mPresenter?.openOrCloseDrawableMenu(!drawerLayout.isDrawerOpen(GravityCompat.START))
    }

    /* VIEW IMPLEMENTS */
    override fun openDrawable() {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    override fun closeDrawable() {
        drawerLayout.closeDrawer(GravityCompat.START)
    }

    override fun mostrarFragment(fragment: Fragment) {
        closeDrawable()
            fragmentSelected = fragment
            supportFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
                .commit()
    }

    override fun clearStack() {
        supportFragmentManager
            .popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun fragmentInit(fragment: Fragment) {
        clearStack()
        fragmentSelected = fragment
        supportFragmentManager
            .beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
            .commit()
    }

    override fun showSearch(show: Boolean) {
        if (!show) {
            layoutSearch.visibility = View.GONE
        } else {
            layoutSearch.visibility = View.VISIBLE
            inputSearch.requestFocus()
            Keyboard.show(this, inputSearch)
        }
    }

    override fun setPresenter(presenter: MainContract.Presenter) {
        this.mPresenter = presenter
    }

    override fun setCallback(callback: IMainView.Callback?) {
        mCallback = callback
    }

    override fun showFab(show: Boolean) {
        fab.visibility =
            if (show) View.VISIBLE
            else View.GONE
    }

    override fun cargarMenu(listDataHeader: ArrayList<ItemMenu>, listDataChild: HashMap<ItemMenu, List<String>>) {
        this.listDataHeader = listDataHeader
        this.listDataChild = listDataChild

        mMenuAdapter = ExpandableListAdapter(applicationContext, listDataHeader!!, listDataChild!!, object : Events {
            override fun onClick(itemId: Int) {
                mPresenter?.onItemMenuSelected(itemId)
            }
        })
        navigationmenu.setAdapter(mMenuAdapter)
    }


}
