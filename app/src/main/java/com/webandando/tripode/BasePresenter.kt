package com.webandando.tripode

interface BasePresenter {
    fun start()
}