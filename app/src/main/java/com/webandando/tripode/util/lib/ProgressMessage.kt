package com.webandando.tripode.util.lib

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import com.budiyev.android.circularprogressbar.CircularProgressBar
import com.squareup.picasso.Picasso
import com.webandando.tripode.R
import de.hdodenhof.circleimageview.CircleImageView

class ProgressMessage(ctx: Context, imagen: String? = null) {
    var alertDialog: AlertDialog? = null
    var builder: AlertDialog.Builder? = null
    var dialogView: View? = null
    private var cpbProgress: CircularProgressBar? = null

    init {
        builder = AlertDialog.Builder(ctx)
        dialogView = LayoutInflater.from(ctx).inflate(R.layout.state_progress_dialog, null)
        if (imagen != null) {
            val loadImg = dialogView!!.findViewById<CircleImageView>(R.id.civ_carga_imagen)
            Picasso
                .get()
                .load(imagen)
                .into(loadImg)
        }
        builder!!.setView(dialogView)
        builder!!.setCancelable(false)
        alertDialog = builder!!.create()
        cpbProgress = dialogView!!.findViewById(R.id.cpb_progress)
    }

    fun show() {
        try {
            alertDialog?.show()
            alertDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hide() {
        alertDialog?.dismiss()
    }
}