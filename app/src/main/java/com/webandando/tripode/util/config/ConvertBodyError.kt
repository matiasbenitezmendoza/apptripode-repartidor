package com.webandando.tripode.util.config

import com.google.gson.Gson
import com.webandando.tripode.data.service.model.ResponseData
import okhttp3.ResponseBody

class ConvertBodyError<T> {

    fun fromResponseBody(responseBody: ResponseBody): ResponseData<T>? {
        val gson = Gson()
        return try {
            gson.fromJson<ResponseData<T>>(responseBody.string(), ResponseData::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}