package com.webandando.tripode.util
import java.util.*

object Fecha {

    private val monthNameEs =
        arrayOf("ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC")
    private val nameDayOfWeekEs = arrayOf("SAB", "DOM", "LUN", "MAR", "MIE", "JUE", "VIE")
    private val monthNameCompleteEs =
            arrayOf("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")

    fun currentMonthString(): String {
        val cal = Calendar.getInstance()
        val month = cal.get(Calendar.MONTH)
        return monthNameEs[month]
    }

    fun currentDayOfWeekString(): String {
        val cal = Calendar.getInstance()
        val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
        return nameDayOfWeekEs[dayOfWeek]
    }

    fun currentDay(): String {
        val cal = Calendar.getInstance()
        val day = cal.get(Calendar.DATE)

        if (day >= 10) return "$day"
        return "0$day"
    }

    fun currentMonth(): String {
        val cal = Calendar.getInstance()
        var month = cal.get(Calendar.MONTH)
        month += 1
        if (month >= 10) return "$month"
        return "0$month"
    }

    fun currentYear(): String {
        val cal = Calendar.getInstance()
        val year = cal.get(Calendar.YEAR)
        return "$year"
    }

    fun currentHour(): String {
        val cal = Calendar.getInstance()
        val hour = cal.get(Calendar.HOUR_OF_DAY)

        if (hour >= 10) return "$hour"
        return "0$hour"
    }

    fun currentMin(): String {
        val cal = Calendar.getInstance()
        val min = cal.get(Calendar.MINUTE)

        if (min >= 10) return "$min"
        return "0$min"
    }

    fun currentSecond(): String {
        val cal = Calendar.getInstance()
        val sec = cal.get(Calendar.SECOND)

        if (sec >= 10) return "$sec"
        return "0$sec"
    }

    //format YYYY-MM-DD HH:SS:MM
    fun currentDateTimeSimple(): String {
        return "${currentYear()}-${currentMonth()}-${currentDay()} ${currentHour()}:${currentMin()}:${currentSecond()}"
    }

    //format YYYY-MM-DD
    fun currentDate(): String {
        return "${currentYear()}-${currentMonth()}-${currentDay()}"
    }

    fun currentYMDHMS(): String {
        return "${currentYear()}${currentMonth()}${currentDay()}${currentHour()}${currentMin()}${currentSecond()}"
    }

    fun formatFechaEs(dia: String, mes: String, anio: String) : String {
        return try {
            "$dia de ${monthNameCompleteEs[mes.toInt() - 1]} de $anio"
        } catch (e : Exception) {
            ""
        }
    }

    fun formatDBFechtoEs(fecha: String) : String {
        val arrFecha = fecha.split("-")
        if (arrFecha.size == 3)
            return "${arrFecha[2]}/${arrFecha[1]}/${arrFecha[0]}"
        return ""
    }

}
