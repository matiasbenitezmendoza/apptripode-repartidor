package com.webandando.tripode.util.config

object Constants {

    const val patternNombre = "^[a-zA-Z\u00f1\u00d1\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da ]+$"
    const val patternPasswd = "^[a-zA-Z0-9_.?!]+$"
    const val patternResidencial = "^[0-9a-zA-Z.,]+$"
    const val patternNumber = "^[0-9]+$"
    const val patternNumberCard = "^d{16}$"
    const val TAG_FRAGMENT = "TAG_FRAGMENT"
    const val URL_HOTS = "https://www.farmaciastripode.com/admin/index.php/api/"

}