package com.webandando.tripode.util.lib

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.webandando.tripode.R
import android.graphics.Typeface
import android.util.TypedValue


class TextDrawable(context: Context, var mText: String) : Drawable() {

    private val DEFAULT_TEXTSIZE = 11f
    private var mPaint : Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mIntrinsicWidth: Int = 0
    private var mIntrinsicHeight: Int = 0

    init {
        val textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            DEFAULT_TEXTSIZE, context.resources.displayMetrics
        )

        mPaint.color = ContextCompat.getColor(context, R.color.scarlet)
        mPaint.isAntiAlias = true
        mPaint.isFakeBoldText = true
        //paint.setShadowLayer(2f, 0f, 0f, android.R.color.transparent)
        mPaint.style = Paint.Style.FILL
        mPaint.textAlign = Paint.Align.CENTER
        mPaint.typeface = Typeface.createFromAsset(context.assets, "fonts/opensans_regular.ttf")

        mPaint.textSize = textSize
        mIntrinsicWidth = (mPaint.measureText(mText, 0, mText.length) + 1.0).toInt()
        mIntrinsicHeight = mPaint.getFontMetricsInt(null)
        bounds.set(0, 0, intrinsicWidth, intrinsicHeight)
    }

    override fun draw(canvas: Canvas) {
        val bounds = bounds
        canvas.drawText(mText, 0, mText.length, bounds.exactCenterX(), bounds.exactCenterY(), mPaint)
    }

    override fun setAlpha(alpha: Int) {
        mPaint.alpha = alpha
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        mPaint.colorFilter = colorFilter
    }

    override fun getIntrinsicWidth(): Int {
        return mIntrinsicWidth
    }

    override fun getIntrinsicHeight(): Int {
        return mIntrinsicHeight
    }


}