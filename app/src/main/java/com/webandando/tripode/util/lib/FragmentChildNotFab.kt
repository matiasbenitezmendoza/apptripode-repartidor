package com.webandando.tripode.util.lib

import android.os.Bundle
import android.view.View

abstract class FragmentChildNotFab : FragmentRoot(), IMainView.Callback {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (context as IMainView.View).setCallback(null)
        (context as IMainView.View).showFab(false)
        (context as IMainView.View).showSearch(false)
    }

    override fun actionFab() { }
}