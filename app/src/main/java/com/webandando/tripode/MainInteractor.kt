package com.webandando.tripode

import android.content.Context
import androidx.fragment.app.Fragment
import com.webandando.tripode.data.menu.ItemMenu
import com.webandando.tripode.data.service.model.ResponseData
import com.webandando.tripode.data.service.model.ResponseListener
import com.webandando.tripode.data.usuario.Usuario
import com.webandando.tripode.data.usuario.source.service.UsuarioService
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.PedidosFragment
import com.webandando.tripode.ui.fragments_menu.historial_pedidos.HPedidosFragment
import com.webandando.tripode.ui.fragments_menu.inicio.InicioFragment
import com.webandando.tripode.ui.fragments_menu.perfil.MenuPerfilFragment

class MainInteractor (private val context: Context) {

    private val serviceUser = UsuarioService.instance(context)

    fun openOrCloseDrawerLayout(cerrado: Boolean, callback: Callback) {
        if (cerrado)
            callback.openDrawer()
        else
            callback.closeDrawer()
    }

    fun menuItemSelected(idItemMenu: Int, callback: Callback) {
        when (idItemMenu) {
            R.id.itemMenuDrawablePerfil -> {
                callback.cambiarFragment(MenuPerfilFragment.instance())
            }
            R.id.itemMenuDrawableInicio -> {
                callback.limpiarPila()
                callback.cambiarFragment(InicioFragment.getInstance())
            }
            R.id.itemMenuDrawablePedidos -> {
                callback.cambiarFragment(PedidosFragment.getInstance())
            }

            R.id.itemMenuDrawableHpedidos -> {
                callback.cambiarFragment(HPedidosFragment.getInstance())
            }
        }
    }

    fun crearMenu(callback: Callback) {
        val listDataHeader = ArrayList<ItemMenu>()
        val listDataChild = HashMap<ItemMenu, List<String>>()

        val menuProfile = ItemMenu()
        menuProfile.nameMenu = context.getString(R.string.menu_mi_perfil)
        menuProfile.icon = R.drawable.ico_profile
        menuProfile.itemId = R.id.itemMenuDrawablePerfil
        listDataHeader.add(menuProfile)

        val menuInicio = ItemMenu()
        menuInicio.nameMenu = context.getString(R.string.menu_drawable_inicio)
        menuInicio.icon = R.drawable.ico_inicio
        menuInicio.itemId = R.id.itemMenuDrawableInicio
        listDataHeader.add(menuInicio)

        val menuPedidos = ItemMenu()
        menuPedidos.nameMenu = context.getString(R.string.menu_drawable_pedidos)
        menuPedidos.icon = R.drawable.ico_marker
        menuPedidos.itemId = R.id.itemMenuDrawablePedidos
        listDataHeader.add(menuPedidos)

        val menuHpedidos = ItemMenu()
        menuHpedidos.nameMenu = context.getString(R.string.menu_drawable_hpedidos)
        menuHpedidos.icon = R.drawable.ico_marker
        menuHpedidos.itemId = R.id.itemMenuDrawableHpedidos
        listDataHeader.add(menuHpedidos)

        val heading1 = ArrayList<String>()
        heading1.add(context.getString(R.string.menu_drawable_medicamentos))
        heading1.add(context.getString(R.string.menu_drawable_bebes))
        heading1.add(context.getString(R.string.menu_drawable_salud_natural))
        heading1.add(context.getString(R.string.menu_drawable_salud_vitaminas))
        heading1.add(context.getString(R.string.menu_drawable_salud_higiene))

      //  listDataChild[listDataHead      //  listDataChild[listDataHeader[6]] = heading1 // Header, Child dataer[6]] = heading1 // Header, Child data
        callback.loadMenu(listDataHeader, listDataChild)
    }

    fun sendToken(token: String, callback: Callback){

        serviceUser.sendToken(token, object : ResponseListener<ResponseData<Usuario>> {
            override fun onSuccess(response: ResponseData<Usuario>) {
                try {
                    callback.enviarToken()
                } catch (e: Exception) {
                    callback.enviarToken()
                }
            }
            override fun onError(t: Throwable) {
                callback.enviarToken()
            }
        })
    }

    interface Callback {
        fun openDrawer()
        fun closeDrawer()
        fun limpiarPila()
        fun enviarToken()
        fun onRequestFailed()
        fun cambiarFragment(fragment: Fragment)
        fun loadMenu(listDataHeader: ArrayList<ItemMenu>, listDataChild: HashMap<ItemMenu, List<String>>)
    }
}