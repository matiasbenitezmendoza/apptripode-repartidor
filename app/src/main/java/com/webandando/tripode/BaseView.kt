package com.webandando.tripode

interface BaseView<T> {
    fun setPresenter(presenter: T)
}