package com.webandando.tripode

import android.util.Log
import androidx.fragment.app.Fragment
import com.webandando.tripode.data.menu.ItemMenu
import com.webandando.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido.UbicacionPedidoFragment
import com.webandando.tripode.ui.fragments_menu.inicio.InicioFragment

class MainPresenter(val mMainView : MainContract.View,
                    val mMainInteractor: MainInteractor) : MainContract.Presenter, MainInteractor.Callback  {

    override fun onRequestFailed() {

    }

    init {
        mMainView.setPresenter(this)
    }

    /* implementacion de contract presenter*/

    override fun start() {
        // mostrar fragmentActual
        mMainInteractor.crearMenu(this)
        mMainView.fragmentInit(InicioFragment.getInstance())

    }

    override fun openOrCloseDrawableMenu(cerrado: Boolean) {
        mMainInteractor.openOrCloseDrawerLayout(cerrado, this)
    }

    override fun onItemMenuSelected(idItemMenu: Int) {
        mMainInteractor.menuItemSelected(idItemMenu, this)
    }

    /* implementacion de callback interactor */
    override fun openDrawer() {
        mMainView.openDrawable()
    }

    override fun closeDrawer() {
        mMainView.closeDrawable()
    }

    override fun cambiarFragment(fragment: Fragment) {
        mMainView.mostrarFragment(fragment)
    }

    override fun limpiarPila() {
        mMainView.clearStack()
    }

    override fun loadMenu(listDataHeader: ArrayList<ItemMenu>, listDataChild: HashMap<ItemMenu, List<String>>) {
        mMainView.cargarMenu(listDataHeader, listDataChild)
    }


    override fun sendToken(token: String) {
        mMainInteractor.sendToken(token, this)
    }

    override fun enviarToken() {
        Log.d("TAG", "Enviado")
    }
}